package eu.primes.dynet.internal.pairwisecompare;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.text.NumberFormat;
import java.util.List;
import java.util.Map;

import javax.swing.DefaultListSelectionModel;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;

import org.cytoscape.model.CyColumn;
import org.cytoscape.model.CyEdge.Type;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;

import eu.primes.dynet.internal.DynamicNetwork;

/**
 * This panel shows information about the current Node colour mapping scheme that has been applied by NodeChangeMapperTask
 * (pairwise network comparison), focusing on one particular node. As it currently is, it displays two tables, one containing
 * the currently analyzed node, the other containing nodes that are direct neighbours of it. Both table shows the original
 * node attribute values in the two network and the calculated log fold-change.
 * 
 * @author Ivan Hendy Goenawan
 */

public class NodeInformationPanel extends JPanel{
	
	private DynamicNetwork dynet;
	private CyNetwork networkA;
	private CyNetwork networkB;
	private String nodeProperty;
	private Map<CyNode, Double> nodeDifferenceMap;
	private CyNode analyzedNode;
	
	
	private JLabel networkALabel;
	private JLabel networkBLabel;
	private JLabel propertyNameLabel;
	private JTable neighbourTable;
	private JTable currentNodeTable;

	public NodeInformationPanel(DynamicNetwork dynet, CyNetwork networkA, CyNetwork networkB, final String nodeProperty, final Map<CyNode, Double> nodeDifferenceMap, final CyNode analyzedNode){
		this.dynet = dynet;
		this.networkA = networkA;
		this.networkB = networkB;
		this.nodeProperty = nodeProperty;
		this.nodeDifferenceMap = nodeDifferenceMap;
		this.analyzedNode = analyzedNode;
		
		
		final String networkAName = networkA.getRow(networkA).get(CyNetwork.NAME, String.class);
		networkALabel = new JLabel(networkAName);
		
		final String networkBName = networkB.getRow(networkB).get(CyNetwork.NAME, String.class);
		networkBLabel = new JLabel(networkBName);
		
		propertyNameLabel = new JLabel(nodeProperty);
		
		final CyNetwork unionNetwork = dynet.getUnionNetwork();
		final List<CyNode> neighbours = unionNetwork.getNeighborList(analyzedNode, Type.ANY);
		while(neighbours.remove(analyzedNode));
		
		neighbourTable = new JTable();
		neighbourTable.setFillsViewportHeight(true);
		neighbourTable.setSelectionMode(DefaultListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		neighbourTable.setCellSelectionEnabled(true);
		
		//this is to allow copying data from the table
		neighbourTable.registerKeyboardAction(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
		        JTable table = (JTable)e.getSource();
				int[] rowsSelected=table.getSelectedRows();
		        int[] colsSelected=table.getSelectedColumns();
		        
		        StringBuffer sbf=new StringBuffer();
		        
		        for (int i = 0; i < rowsSelected.length; i++){
		        	for (int j = 0; j < colsSelected.length; j++){
		        		sbf.append(table.getValueAt(rowsSelected[i],colsSelected[j]));
		        		if (j < colsSelected.length - 1) sbf.append("\t");
		            }
		            sbf.append("\n");
		        }
		        StringSelection stsel  = new StringSelection(sbf.toString());
		        Clipboard systemClipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		        systemClipboard.setContents(stsel,stsel);
			}
		}, KeyStroke.getKeyStroke(KeyEvent.VK_C,ActionEvent.CTRL_MASK,false), JComponent.WHEN_FOCUSED);
		neighbourTable.setDefaultRenderer(Double.class, new DefaultTableCellRenderer(){
			{
				setHorizontalAlignment(SwingConstants.RIGHT);
			}
			@Override
			protected void setValue(Object value) {
				if (value != null){
					Double doubleValue = (Double) value;
					if (doubleValue > Double.NEGATIVE_INFINITY && doubleValue < Double.POSITIVE_INFINITY){
						setText(doubleValue.toString());
					}else{
						setText(NumberFormat.getInstance().format(value));
					}
				}else{
					setText("");
				}
			}
		});
		neighbourTable.setModel(new AbstractTableModel() {
			
			@Override
			public Object getValueAt(int rowIndex, int columnIndex) {
				CyNode node = neighbours.get(rowIndex);
				
				switch(columnIndex){
				case 0:
					return unionNetwork.getRow(node).get(CyNetwork.NAME, String.class);
				case 1:
					return unionNetwork.getRow(node).get(networkAName + "_" + nodeProperty, Object.class);
				case 2:
					return unionNetwork.getRow(node).get(networkBName + "_" + nodeProperty, Object.class);
				case 3:
					return nodeDifferenceMap.get(node);
				default:
					return null;
				}
			}
			
			@Override
			public int getRowCount() {
				return neighbours.size();
			}
			
			@Override
			public int getColumnCount() {
				return 4;
			}

			@Override
			public Class<?> getColumnClass(int columnIndex) {
				switch (columnIndex){
				case 0:
					return String.class;
				case 3:
					return Double.class;
				case 1:
					CyColumn columnA = unionNetwork.getDefaultNodeTable().getColumn(networkAName + "_" + nodeProperty);
					if (columnA != null) return columnA.getType();
				case 2:
					CyColumn columnB = unionNetwork.getDefaultNodeTable().getColumn(networkBName + "_" + nodeProperty);
					if (columnB != null) return columnB.getType();
				default:
					return super.getColumnClass(columnIndex);
				}
			}
		});
		neighbourTable.setAutoCreateRowSorter(true);
		neighbourTable.getRowSorter().toggleSortOrder(0);
		
		JTableHeader neighbourTableHeader = neighbourTable.getTableHeader();
		neighbourTableHeader.getColumnModel().getColumn(0).setHeaderValue("Nodes");
		neighbourTableHeader.getColumnModel().getColumn(1).setHeaderValue(networkAName);
		neighbourTableHeader.getColumnModel().getColumn(2).setHeaderValue(networkBName);
		neighbourTableHeader.getColumnModel().getColumn(3).setHeaderValue("Log2 fold-change");
		
		
		
		
		currentNodeTable = new JTable();
		currentNodeTable.setFillsViewportHeight(true);
		currentNodeTable.setSelectionMode(DefaultListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		currentNodeTable.setCellSelectionEnabled(true);
		
		//this is to allow copying data from the table
		currentNodeTable.registerKeyboardAction(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
		        JTable table = (JTable)e.getSource();
				int[] rowsSelected=table.getSelectedRows();
		        int[] colsSelected=table.getSelectedColumns();
		        
		        StringBuffer sbf=new StringBuffer();
		        
		        for (int i = 0; i < rowsSelected.length; i++){
		        	for (int j = 0; j < colsSelected.length; j++){
		        		sbf.append(table.getValueAt(rowsSelected[i],colsSelected[j]));
		        		if (j < colsSelected.length - 1) sbf.append("\t");
		            }
		            sbf.append("\n");
		        }
		        StringSelection stsel  = new StringSelection(sbf.toString());
		        Clipboard systemClipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		        systemClipboard.setContents(stsel,stsel);
			}
		}, KeyStroke.getKeyStroke(KeyEvent.VK_C,ActionEvent.CTRL_MASK,false), JComponent.WHEN_FOCUSED);
		currentNodeTable.setDefaultRenderer(Double.class, new DefaultTableCellRenderer(){
			{
				setHorizontalAlignment(SwingConstants.RIGHT);
			}
			@Override
			protected void setValue(Object value) {
				if (value != null){
					Double doubleValue = (Double) value;
					if (doubleValue > Double.NEGATIVE_INFINITY && doubleValue < Double.POSITIVE_INFINITY){
						setText(doubleValue.toString());
					}else{
						setText(NumberFormat.getInstance().format(value));
					}
				}else{
					setText("");
				}
			}
		});
		currentNodeTable.setModel(new AbstractTableModel() {
			
			@Override
			public Object getValueAt(int rowIndex, int columnIndex) {
				switch(columnIndex){
				case 0:
					return unionNetwork.getRow(analyzedNode).get(CyNetwork.NAME, String.class);
				case 1:
					return unionNetwork.getRow(analyzedNode).get(networkAName + "_" + nodeProperty, Object.class);
				case 2:
					return unionNetwork.getRow(analyzedNode).get(networkBName + "_" + nodeProperty, Object.class);
				case 3:
					return nodeDifferenceMap.get(analyzedNode);
				default:
					return null;
				}
			}
			
			@Override
			public int getRowCount() {
				return 1;
			}
			
			@Override
			public int getColumnCount() {
				return 4;
			}

			@Override
			public Class<?> getColumnClass(int columnIndex) {
				switch (columnIndex){
				case 0:
					return String.class;
				case 3:
					return Double.class;
				case 1:
					CyColumn columnA = unionNetwork.getDefaultNodeTable().getColumn(networkAName + "_" + nodeProperty);
					if (columnA != null) return columnA.getType();
				case 2:
					CyColumn columnB = unionNetwork.getDefaultNodeTable().getColumn(networkBName + "_" + nodeProperty);
					if (columnB != null) return columnB.getType();
				default:
					return super.getColumnClass(columnIndex);
				}
			}
		});
		
		JTableHeader currentNodeTableHeader = currentNodeTable.getTableHeader();
		currentNodeTableHeader.getColumnModel().getColumn(0).setHeaderValue("Node");
		currentNodeTableHeader.getColumnModel().getColumn(1).setHeaderValue(networkAName);
		currentNodeTableHeader.getColumnModel().getColumn(2).setHeaderValue(networkBName);
		currentNodeTableHeader.getColumnModel().getColumn(3).setHeaderValue("Log2 fold-change");
		
		
		
		
		setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{10, 25, 0, 120, 0, 10, 0};
		gridBagLayout.rowHeights = new int[]{10, 40, 25, 25, 30, 20, 30, 20, 10, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		JLabel titleLabel = new JLabel("Node colour currently mapped to pairwise network comparison:");
		GridBagConstraints gbc_titleLabel = new GridBagConstraints();
		gbc_titleLabel.gridwidth = 4;
		gbc_titleLabel.anchor = GridBagConstraints.NORTHWEST;
		gbc_titleLabel.insets = new Insets(0, 0, 5, 5);
		gbc_titleLabel.gridx = 1;
		gbc_titleLabel.gridy = 1;
		add(titleLabel, gbc_titleLabel);
		
		JPanel greenPanel = new JPanel();
		greenPanel.setBackground(Color.GREEN);
		GridBagConstraints gbc_greenPanel = new GridBagConstraints();
		gbc_greenPanel.insets = new Insets(0, 0, 5, 5);
		gbc_greenPanel.fill = GridBagConstraints.BOTH;
		gbc_greenPanel.gridx = 1;
		gbc_greenPanel.gridy = 2;
		add(greenPanel, gbc_greenPanel);
		
		
		GridBagConstraints gbc_networkALabel = new GridBagConstraints();
		gbc_networkALabel.anchor = GridBagConstraints.WEST;
		gbc_networkALabel.insets = new Insets(0, 0, 5, 5);
		gbc_networkALabel.gridx = 2;
		gbc_networkALabel.gridy = 2;
		add(networkALabel, gbc_networkALabel);
		
		JLabel nodePropertyLabel = new JLabel("Node property:");
		GridBagConstraints gbc_nodePropertyLabel = new GridBagConstraints();
		gbc_nodePropertyLabel.anchor = GridBagConstraints.WEST;
		gbc_nodePropertyLabel.fill = GridBagConstraints.VERTICAL;
		gbc_nodePropertyLabel.insets = new Insets(0, 0, 5, 5);
		gbc_nodePropertyLabel.gridx = 4;
		gbc_nodePropertyLabel.gridy = 2;
		add(nodePropertyLabel, gbc_nodePropertyLabel);
		
		JPanel redPanel = new JPanel();
		redPanel.setBackground(Color.RED);
		GridBagConstraints gbc_redPanel = new GridBagConstraints();
		gbc_redPanel.insets = new Insets(0, 0, 5, 5);
		gbc_redPanel.fill = GridBagConstraints.BOTH;
		gbc_redPanel.gridx = 1;
		gbc_redPanel.gridy = 3;
		add(redPanel, gbc_redPanel);
		
	
		GridBagConstraints gbc_networkBLabel = new GridBagConstraints();
		gbc_networkBLabel.anchor = GridBagConstraints.WEST;
		gbc_networkBLabel.insets = new Insets(0, 0, 5, 5);
		gbc_networkBLabel.gridx = 2;
		gbc_networkBLabel.gridy = 3;
		add(networkBLabel, gbc_networkBLabel);
		
		
		GridBagConstraints gbc_propertyNameLabel = new GridBagConstraints();
		gbc_propertyNameLabel.anchor = GridBagConstraints.WEST;
		gbc_propertyNameLabel.insets = new Insets(0, 0, 5, 5);
		gbc_propertyNameLabel.gridx = 4;
		gbc_propertyNameLabel.gridy = 3;
		add(propertyNameLabel, gbc_propertyNameLabel);
		
		JLabel currentNodeLabel = new JLabel("Current node:");
		GridBagConstraints gbc_currentNodeLabel = new GridBagConstraints();
		gbc_currentNodeLabel.anchor = GridBagConstraints.SOUTHWEST;
		gbc_currentNodeLabel.gridwidth = 4;
		gbc_currentNodeLabel.insets = new Insets(0, 0, 5, 5);
		gbc_currentNodeLabel.gridx = 1;
		gbc_currentNodeLabel.gridy = 4;
		add(currentNodeLabel, gbc_currentNodeLabel);
		
		JPanel currentNodeTablePanel = new JPanel();
		GridBagConstraints gbc_currentNodeTablePanel = new GridBagConstraints();
		gbc_currentNodeTablePanel.gridwidth = 4;
		gbc_currentNodeTablePanel.insets = new Insets(0, 0, 5, 5);
		gbc_currentNodeTablePanel.fill = GridBagConstraints.BOTH;
		gbc_currentNodeTablePanel.gridx = 1;
		gbc_currentNodeTablePanel.gridy = 5;
		add(currentNodeTablePanel, gbc_currentNodeTablePanel);
		currentNodeTablePanel.setLayout(new BorderLayout(0, 0));
		currentNodeTablePanel.add(currentNodeTableHeader, BorderLayout.NORTH);
		currentNodeTablePanel.add(currentNodeTable, BorderLayout.CENTER);
		
		JLabel firstNeighboursLabel = new JLabel("First neighbours:");
		GridBagConstraints gbc_firstNeighboursLabel = new GridBagConstraints();
		gbc_firstNeighboursLabel.anchor = GridBagConstraints.SOUTHWEST;
		gbc_firstNeighboursLabel.gridwidth = 4;
		gbc_firstNeighboursLabel.insets = new Insets(0, 0, 5, 5);
		gbc_firstNeighboursLabel.gridx = 1;
		gbc_firstNeighboursLabel.gridy = 6;
		add(firstNeighboursLabel, gbc_firstNeighboursLabel);
		
		JPanel neighbourTablePanel = new JPanel();
		GridBagConstraints gbc_neighbourTablePanel = new GridBagConstraints();
		gbc_neighbourTablePanel.gridwidth = 4;
		gbc_neighbourTablePanel.insets = new Insets(0, 0, 5, 5);
		gbc_neighbourTablePanel.fill = GridBagConstraints.BOTH;
		gbc_neighbourTablePanel.gridx = 1;
		gbc_neighbourTablePanel.gridy = 7;
		add(neighbourTablePanel, gbc_neighbourTablePanel);
		neighbourTablePanel.setLayout(new BorderLayout(0, 0));
		neighbourTablePanel.add(neighbourTableHeader, BorderLayout.NORTH);
		neighbourTablePanel.add(neighbourTable, BorderLayout.CENTER);
	}
}
