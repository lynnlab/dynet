package eu.primes.dynet.internal.pairwisecompare;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.text.NumberFormat;
import java.util.List;
import java.util.Map;

import javax.swing.DefaultListSelectionModel;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;

import org.cytoscape.model.CyColumn;
import org.cytoscape.model.CyEdge;
import org.cytoscape.model.CyEdge.Type;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;

import eu.primes.dynet.internal.DynamicNetwork;

/**
 * This panel shows information about the current Edge colour mapping scheme that has been applied by EdgeChangeMapperTask
 * (pairwise network comparison), focusing on one particular node. As it currently is, it displays a table listing the
 * edges adjacent to the node being analyzed, their values in the two networks (for the attribute currently used 
 * for the mapping), and the calculated log fold-change.
 * 
 * @author Ivan Hendy Goenawan
 */


public class EdgeInformationPanel extends JPanel{
	
	private DynamicNetwork dynet;
	private CyNetwork networkA;
	private CyNetwork networkB;
	private String edgeProperty;
	private Map<CyEdge, Double> edgeDifferenceMap;
	private CyNode analyzedNode;
	
	
	private JLabel networkALabel;
	private JLabel networkBLabel;
	private JLabel propertyNameLabel;
	private JTable edgeTable;

	public EdgeInformationPanel(DynamicNetwork dynet, CyNetwork networkA, CyNetwork networkB, final String edgeProperty, final Map<CyEdge, Double> edgeDifferenceMap, final CyNode analyzedNode){
		this.dynet = dynet;
		this.networkA = networkA;
		this.networkB = networkB;
		this.edgeProperty = edgeProperty;
		this.edgeDifferenceMap = edgeDifferenceMap;
		this.analyzedNode = analyzedNode;
		
		
		final String networkAName = networkA.getRow(networkA).get(CyNetwork.NAME, String.class);
		networkALabel = new JLabel(networkAName);
		
		final String networkBName = networkB.getRow(networkB).get(CyNetwork.NAME, String.class);
		networkBLabel = new JLabel(networkBName);
		
		propertyNameLabel = new JLabel(edgeProperty);
		
		final CyNetwork unionNetwork = dynet.getUnionNetwork();
		final List<CyEdge> adjacentEdges = unionNetwork.getAdjacentEdgeList(analyzedNode, Type.ANY);
		
		edgeTable = new JTable();
		edgeTable.setFillsViewportHeight(true);
		edgeTable.setSelectionMode(DefaultListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		edgeTable.setCellSelectionEnabled(true);
		
		//this is to enable copying data from the table
		edgeTable.registerKeyboardAction(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
		        JTable table = (JTable)e.getSource();
				int[] rowsSelected=table.getSelectedRows();
		        int[] colsSelected=table.getSelectedColumns();
		        
		        StringBuffer sbf=new StringBuffer();
		        
		        for (int i = 0; i < rowsSelected.length; i++){
		        	for (int j = 0; j < colsSelected.length; j++){
		        		sbf.append(table.getValueAt(rowsSelected[i],colsSelected[j]));
		        		if (j < colsSelected.length - 1) sbf.append("\t");
		            }
		            sbf.append("\n");
		        }
		        StringSelection stsel  = new StringSelection(sbf.toString());
		        Clipboard systemClipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		        systemClipboard.setContents(stsel,stsel);
			}
		}, KeyStroke.getKeyStroke(KeyEvent.VK_C,ActionEvent.CTRL_MASK,false), JComponent.WHEN_FOCUSED);
		edgeTable.setDefaultRenderer(Double.class, new DefaultTableCellRenderer(){
			{
				setHorizontalAlignment(SwingConstants.RIGHT);
			}
			@Override
			protected void setValue(Object value) {
				if (value != null){
					Double doubleValue = (Double) value;
					if (doubleValue > Double.NEGATIVE_INFINITY && doubleValue < Double.POSITIVE_INFINITY){
						setText(doubleValue.toString());
					}else{
						setText(NumberFormat.getInstance().format(value));
					}
				}else{
					setText("");
				}
			}
		});
		edgeTable.setModel(new AbstractTableModel() {
			
			@Override
			public Object getValueAt(int rowIndex, int columnIndex) {
				CyEdge edge = adjacentEdges.get(rowIndex);
				CyNode source = edge.getSource();
				CyNode target = edge.getTarget();
				CyNode neighbor = (source == analyzedNode) ? target : source;
				
				switch(columnIndex){
				case 0:
					return unionNetwork.getRow(neighbor).get(CyNetwork.NAME, String.class);
				case 1:
					return unionNetwork.getRow(edge).get(networkAName + "_" + edgeProperty, Object.class);
				case 2:
					return unionNetwork.getRow(edge).get(networkBName + "_" + edgeProperty, Object.class);
				case 3:
					return edgeDifferenceMap.get(edge);
				default:
					return null;
				}
			}
			
			@Override
			public int getRowCount() {
				return adjacentEdges.size();
			}
			
			@Override
			public int getColumnCount() {
				return 4;
			}

			@Override
			public Class<?> getColumnClass(int columnIndex) {
				switch (columnIndex){
				case 0:
					return String.class;
				case 3:
					return Double.class;
				case 1:
					CyColumn columnA = unionNetwork.getDefaultEdgeTable().getColumn(networkAName + "_" + edgeProperty);
					if (columnA != null) return columnA.getType();
				case 2:
					CyColumn columnB = unionNetwork.getDefaultEdgeTable().getColumn(networkBName + "_" + edgeProperty);
					if (columnB != null) return columnB.getType();
				default:
					return super.getColumnClass(columnIndex);
				}
			}
		});
		edgeTable.setAutoCreateRowSorter(true);
		edgeTable.getRowSorter().toggleSortOrder(0);
		
		JTableHeader tableHeader = edgeTable.getTableHeader();
		tableHeader.getColumnModel().getColumn(0).setHeaderValue("Edge to");
		tableHeader.getColumnModel().getColumn(1).setHeaderValue(networkAName);
		tableHeader.getColumnModel().getColumn(2).setHeaderValue(networkBName);
		tableHeader.getColumnModel().getColumn(3).setHeaderValue("Log2 fold-change");
		
		
		setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{10, 25, 0, 120, 0, 10, 0};
		gridBagLayout.rowHeights = new int[]{10, 40, 25, 25, 20, 20, 10, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		JLabel titleLabel = new JLabel("Edge colour currently mapped to pairwise network comparison:");
		GridBagConstraints gbc_titleLabel = new GridBagConstraints();
		gbc_titleLabel.gridwidth = 4;
		gbc_titleLabel.anchor = GridBagConstraints.NORTHWEST;
		gbc_titleLabel.insets = new Insets(0, 0, 5, 5);
		gbc_titleLabel.gridx = 1;
		gbc_titleLabel.gridy = 1;
		add(titleLabel, gbc_titleLabel);
		
		JPanel greenPanel = new JPanel();
		greenPanel.setBackground(Color.GREEN);
		GridBagConstraints gbc_greenPanel = new GridBagConstraints();
		gbc_greenPanel.insets = new Insets(0, 0, 5, 5);
		gbc_greenPanel.fill = GridBagConstraints.BOTH;
		gbc_greenPanel.gridx = 1;
		gbc_greenPanel.gridy = 2;
		add(greenPanel, gbc_greenPanel);
		
		
		GridBagConstraints gbc_networkALabel = new GridBagConstraints();
		gbc_networkALabel.anchor = GridBagConstraints.WEST;
		gbc_networkALabel.insets = new Insets(0, 0, 5, 5);
		gbc_networkALabel.gridx = 2;
		gbc_networkALabel.gridy = 2;
		add(networkALabel, gbc_networkALabel);
		
		JLabel nodePropertyLabel = new JLabel("Edge property:");
		GridBagConstraints gbc_nodePropertyLabel = new GridBagConstraints();
		gbc_nodePropertyLabel.anchor = GridBagConstraints.WEST;
		gbc_nodePropertyLabel.fill = GridBagConstraints.VERTICAL;
		gbc_nodePropertyLabel.insets = new Insets(0, 0, 5, 5);
		gbc_nodePropertyLabel.gridx = 4;
		gbc_nodePropertyLabel.gridy = 2;
		add(nodePropertyLabel, gbc_nodePropertyLabel);
		
		JPanel redPanel = new JPanel();
		redPanel.setBackground(Color.RED);
		GridBagConstraints gbc_redPanel = new GridBagConstraints();
		gbc_redPanel.insets = new Insets(0, 0, 5, 5);
		gbc_redPanel.fill = GridBagConstraints.BOTH;
		gbc_redPanel.gridx = 1;
		gbc_redPanel.gridy = 3;
		add(redPanel, gbc_redPanel);
		
	
		GridBagConstraints gbc_networkBLabel = new GridBagConstraints();
		gbc_networkBLabel.anchor = GridBagConstraints.WEST;
		gbc_networkBLabel.insets = new Insets(0, 0, 5, 5);
		gbc_networkBLabel.gridx = 2;
		gbc_networkBLabel.gridy = 3;
		add(networkBLabel, gbc_networkBLabel);
		
		
		GridBagConstraints gbc_propertyNameLabel = new GridBagConstraints();
		gbc_propertyNameLabel.anchor = GridBagConstraints.WEST;
		gbc_propertyNameLabel.insets = new Insets(0, 0, 5, 5);
		gbc_propertyNameLabel.gridx = 4;
		gbc_propertyNameLabel.gridy = 3;
		add(propertyNameLabel, gbc_propertyNameLabel);
		
		JPanel nodeTablePanel = new JPanel();
		GridBagConstraints gbc_nodeTablePanel = new GridBagConstraints();
		gbc_nodeTablePanel.gridwidth = 4;
		gbc_nodeTablePanel.insets = new Insets(0, 0, 5, 5);
		gbc_nodeTablePanel.fill = GridBagConstraints.BOTH;
		gbc_nodeTablePanel.gridx = 1;
		gbc_nodeTablePanel.gridy = 5;
		add(nodeTablePanel, gbc_nodeTablePanel);
		nodeTablePanel.setLayout(new BorderLayout(0, 0));
		nodeTablePanel.add(tableHeader, BorderLayout.NORTH);
		nodeTablePanel.add(edgeTable, BorderLayout.CENTER);
	}
}
