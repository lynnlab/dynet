package eu.primes.dynet.internal.pairwisecompare;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.cytoscape.app.CyAppAdapter;
import org.cytoscape.model.CyEdge;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;
import org.cytoscape.model.CyTable;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.model.View;
import org.cytoscape.view.presentation.property.BasicVisualLexicon;
import org.cytoscape.work.TaskIterator;

import eu.primes.dynet.internal.ControlPanel;
import eu.primes.dynet.internal.ControlPanel.ControlPanelComponent;
import eu.primes.dynet.internal.ControlPanel.EdgeColourController;
import eu.primes.dynet.internal.ControlPanel.NodeColourController;
import eu.primes.dynet.internal.DynamicNetwork;

/**
 * This Control Panel component provides the menu for highlighting nodes and edges based on differences between two
 * networks (pairwise network comparison). The user can choose any numerical or boolean attribute to compare.
 * The app will associate the colour red and green to each network. Nodes/edges that has higher value in the 'green' network
 * will have greener colour, and likewise with the 'red' network. If the values are the same in both networks, then the
 * node/edge colour won't change. 
 * 
 * As both an EdgeColourController and NodeColourController, it can also supply a JPanel containing information about
 * the current colour mapping that is being applied.
 * 
 * @author Ivan Hendy Goenawan
 */

public class PairwiseComparePanel extends ControlPanelComponent implements EdgeColourController, NodeColourController {
		private ControlPanel controlPanel;
		private DynamicNetwork dynet;
		private CyAppAdapter appAdapter;
		
		private JCheckBox highlightNodesCheckBox;
		private JCheckBox highlightEdgesCheckBox;
		private JComboBox<CyNetwork> networkAComboBox;
		private JComboBox<CyNetwork> networkBComboBox;
		private JComboBox<String> edgePropertyComboBox;
		private JComboBox<String> nodePropertyComboBox;
		private JCheckBox ignoreAbsenceCheckBox;
		
		private JPanel greenBoxPanel;
		private JPanel redBoxPanel;
		private JLabel nodePropertyLabel;
		private JLabel edgePropertyLabel;
		
		private Map<CyEdge, Double> edgeDifferenceMap;
		private Map<CyNode, Double> nodeDifferenceMap;
		
		boolean ignoreAbsence;
		boolean highlightNodesEnabled;
		boolean highlightEdgesEnabled;
		private CyNetwork networkA;
		private CyNetwork networkB;
		private String edgeProperty;
		private String nodeProperty;
		
		private static final String NODE_RESULT_COLUMN_NAME = "DyNet Pairwise Comparison";
		private static final String EDGE_RESULT_COLUMN_NAME = "DyNet Pairwise Comparison";
		private JSeparator separator;
		private JLabel additionalOptionsLabel;
		
		
		
		public PairwiseComparePanel (ControlPanel controlPanel, DynamicNetwork dynet, CyAppAdapter appAdapter, boolean defaultNodesEnabled, boolean defaultEdgesEnabled){
			this.controlPanel = controlPanel;
			this.dynet = dynet;
			this.appAdapter = appAdapter;
			this.highlightNodesEnabled = defaultNodesEnabled;
			this.highlightEdgesEnabled = defaultEdgesEnabled;
			
			
			networkAComboBox = new JComboBox<CyNetwork>(dynet.getMemberNetworks().toArray(new CyNetwork[0]));
			networkAComboBox.setSelectedIndex(0);
			networkA = networkAComboBox.getItemAt(0);
			networkAComboBox.addItemListener(new ItemListener() {
				@Override
				public void itemStateChanged(ItemEvent e) {
					if (e.getStateChange() == ItemEvent.SELECTED){
						networkA = (CyNetwork)e.getItem();
						update();
					}else if (e.getStateChange() == ItemEvent.DESELECTED){
						CyNetwork oldNetworkA = (CyNetwork)e.getItem();
						for (CyNetworkView oldNetworkAView : PairwiseComparePanel.this.appAdapter.getCyNetworkViewManager().getNetworkViews(oldNetworkA)){
							if (highlightNodesEnabled){
								for (View<CyNode> nodeView : oldNetworkAView.getNodeViews()){
									nodeView.clearValueLock(BasicVisualLexicon.NODE_FILL_COLOR);
									nodeView.clearValueLock(BasicVisualLexicon.NODE_SIZE);
								}
							}
							
							if (highlightEdgesEnabled){
								for (View<CyEdge> edgeView : oldNetworkAView.getEdgeViews()){
									edgeView.clearValueLock(BasicVisualLexicon.EDGE_STROKE_UNSELECTED_PAINT);
									edgeView.clearValueLock(BasicVisualLexicon.EDGE_WIDTH);
								}
							}
							
							oldNetworkAView.updateView();
						}
					}
				}
			});
			
			
			networkBComboBox = new JComboBox<CyNetwork>(dynet.getMemberNetworks().toArray(new CyNetwork[0]));
			networkBComboBox.setSelectedIndex(1);
			networkB = networkBComboBox.getItemAt(1);
			networkBComboBox.addItemListener(new ItemListener() {
				@Override
				public void itemStateChanged(ItemEvent e) {
					if (e.getStateChange() == ItemEvent.SELECTED){
						networkB = (CyNetwork)e.getItem();
						update();
						
					}else if (e.getStateChange() == ItemEvent.DESELECTED){
						CyNetwork oldNetworkB = (CyNetwork)e.getItem();
						for (CyNetworkView oldNetworkBView : PairwiseComparePanel.this.appAdapter.getCyNetworkViewManager().getNetworkViews(oldNetworkB)){
							if (highlightNodesEnabled){
								for (View<CyNode> nodeView : oldNetworkBView.getNodeViews()){
									nodeView.clearValueLock(BasicVisualLexicon.NODE_FILL_COLOR);
									nodeView.clearValueLock(BasicVisualLexicon.NODE_SIZE);
								}
							}
							
							if (highlightEdgesEnabled){
								for (View<CyEdge> edgeView : oldNetworkBView.getEdgeViews()){
									edgeView.clearValueLock(BasicVisualLexicon.EDGE_STROKE_UNSELECTED_PAINT);
									edgeView.clearValueLock(BasicVisualLexicon.EDGE_WIDTH);
								}
							}
							
							oldNetworkBView.updateView();
						}
					}
				}
			});
			
			
			ignoreAbsenceCheckBox = new JCheckBox("<html>Only highlight common but<br>attribute-varying nodes/edges</html>");
			ignoreAbsenceCheckBox.setSelected(false);
			ignoreAbsence = false;
			ignoreAbsenceCheckBox.addItemListener(new ItemListener() {
				@Override
				public void itemStateChanged(ItemEvent e) {
					if (e.getStateChange() == ItemEvent.SELECTED){
						ignoreAbsence = true;
						update();
					}else if (e.getStateChange() == ItemEvent.DESELECTED){
						ignoreAbsence = false;
						update();
					}
				}
			});
			
			
			
			highlightNodesCheckBox = new JCheckBox("Highlight node changes");
			highlightNodesCheckBox.setSelected(highlightNodesEnabled);
			if (highlightNodesEnabled){
				controlPanel.takeNodeColourControl(this);
			}
			highlightNodesCheckBox.addItemListener(new ItemListener() {
				@Override
				public void itemStateChanged(ItemEvent e) {
					if (e.getStateChange() == ItemEvent.SELECTED){
						highlightNodesEnabled = true;
						PairwiseComparePanel.this.controlPanel.takeNodeColourControl(PairwiseComparePanel.this);
						
						if (nodeProperty.equals(DynamicNetwork.PRESENT) && ignoreAbsence){
							ignoreAbsenceCheckBox.setSelected(false);
							return;  //update() is already called by ignoreAbsenceCheckBox
						}
						
						update();
					}else if (e.getStateChange() == ItemEvent.DESELECTED){
						highlightNodesEnabled = false;
						
						for(View<CyNode> nodeView : PairwiseComparePanel.this.dynet.getUnionNetworkView().getNodeViews()){
							nodeView.clearValueLock(BasicVisualLexicon.NODE_FILL_COLOR);
							nodeView.clearValueLock(BasicVisualLexicon.NODE_SIZE);
						}
						PairwiseComparePanel.this.dynet.getUnionNetworkView().updateView();
						
						for (CyNetworkView networkAView : PairwiseComparePanel.this.appAdapter.getCyNetworkViewManager().getNetworkViews(networkA)){
							for (View<CyNode> nodeView : networkAView.getNodeViews()){
								nodeView.clearValueLock(BasicVisualLexicon.NODE_FILL_COLOR);
								nodeView.clearValueLock(BasicVisualLexicon.NODE_SIZE);
							}
							networkAView.updateView();
						}
						
						for (CyNetworkView networkBView : PairwiseComparePanel.this.appAdapter.getCyNetworkViewManager().getNetworkViews(networkB)){
							for (View<CyNode> nodeView : networkBView.getNodeViews()){
								nodeView.clearValueLock(BasicVisualLexicon.NODE_FILL_COLOR);
								nodeView.clearValueLock(BasicVisualLexicon.NODE_SIZE);
							}
							networkBView.updateView();
						}
						
						
						CyTable nodeTable = PairwiseComparePanel.this.dynet.getUnionNetwork().getDefaultNodeTable();
						nodeTable.deleteColumn(NODE_RESULT_COLUMN_NAME);
						
						
						PairwiseComparePanel.this.controlPanel.releaseNodeColourControl(PairwiseComparePanel.this);
					}
					
				}
			});
			
			
			List<String> ungroupedNodeAttributes = dynet.getUngroupedNodeAttributes();
			Collections.sort(ungroupedNodeAttributes);
			nodeProperty = DynamicNetwork.PRESENT;
			nodePropertyComboBox = new JComboBox<String>(ungroupedNodeAttributes.toArray(new String[0]));
			nodePropertyComboBox.setSelectedIndex(ungroupedNodeAttributes.indexOf(DynamicNetwork.PRESENT));
			nodePropertyComboBox.addItemListener(new ItemListener() {
				@Override
				public void itemStateChanged(ItemEvent e) {
					if (e.getStateChange()  == ItemEvent.SELECTED){
						nodeProperty = (String)e.getItem();
						if (nodeProperty.equals(DynamicNetwork.PRESENT) && ignoreAbsence){
							ignoreAbsenceCheckBox.setSelected(false);
							return;  //update() is already called by ignoreAbsenceCheckBox
						}
						update();
					}
				}
			});
			
			
			
			highlightEdgesCheckBox = new JCheckBox("Highlight edge changes");
			highlightEdgesCheckBox.setSelected(highlightEdgesEnabled);
			if (highlightEdgesEnabled){
				controlPanel.takeEdgeColourControl(this);
			}
			highlightEdgesCheckBox.addItemListener(new ItemListener() {
				@Override
				public void itemStateChanged(ItemEvent e) {
					if (e.getStateChange() == ItemEvent.SELECTED){
						highlightEdgesEnabled = true;
						PairwiseComparePanel.this.controlPanel.takeEdgeColourControl(PairwiseComparePanel.this);
						
						if (edgeProperty.equals(DynamicNetwork.PRESENT) && ignoreAbsence){
							ignoreAbsenceCheckBox.setSelected(false);
							return;  //update() is already called by ignoreAbsenceCheckBox
						}
						
						update();
					}else if (e.getStateChange() == ItemEvent.DESELECTED){
						highlightEdgesEnabled = false;
						
						for(View<CyEdge> edgeView : PairwiseComparePanel.this.dynet.getUnionNetworkView().getEdgeViews()){
							edgeView.clearValueLock(BasicVisualLexicon.EDGE_STROKE_UNSELECTED_PAINT);
							edgeView.clearValueLock(BasicVisualLexicon.EDGE_WIDTH);
						}
						PairwiseComparePanel.this.dynet.getUnionNetworkView().updateView();
						
						
						for (CyNetworkView networkAView : PairwiseComparePanel.this.appAdapter.getCyNetworkViewManager().getNetworkViews(networkA)){
							for (View<CyEdge> edgeView : networkAView.getEdgeViews()){
								edgeView.clearValueLock(BasicVisualLexicon.EDGE_STROKE_UNSELECTED_PAINT);
								edgeView.clearValueLock(BasicVisualLexicon.EDGE_WIDTH);
							}
							networkAView.updateView();
						}
						
						for (CyNetworkView networkBView : PairwiseComparePanel.this.appAdapter.getCyNetworkViewManager().getNetworkViews(networkB)){
							for (View<CyEdge> edgeView : networkBView.getEdgeViews()){
								edgeView.clearValueLock(BasicVisualLexicon.EDGE_STROKE_UNSELECTED_PAINT);
								edgeView.clearValueLock(BasicVisualLexicon.EDGE_WIDTH);
							}
							networkBView.updateView();
						}
						
						
						CyTable edgeTable = PairwiseComparePanel.this.dynet.getUnionNetwork().getDefaultEdgeTable();
						edgeTable.deleteColumn(EDGE_RESULT_COLUMN_NAME);
						
						
						PairwiseComparePanel.this.controlPanel.releaseEdgeColourControl(PairwiseComparePanel.this);
					}
				}
			});
			
			
			List<String> ungroupedEdgeAttributes = dynet.getUngroupedEdgeAttributes();
			Collections.sort(ungroupedEdgeAttributes);
			edgeProperty = DynamicNetwork.PRESENT;
			edgePropertyComboBox = new JComboBox<String>(ungroupedEdgeAttributes.toArray(new String[0]));
			edgePropertyComboBox.setSelectedIndex(ungroupedEdgeAttributes.indexOf(DynamicNetwork.PRESENT));
			edgePropertyComboBox.addItemListener(new ItemListener() {
				@Override
				public void itemStateChanged(ItemEvent e) {
					if (e.getStateChange() == ItemEvent.SELECTED){
						edgeProperty = (String)e.getItem();
						if (edgeProperty.equals(DynamicNetwork.PRESENT) && ignoreAbsence){
							ignoreAbsenceCheckBox.setSelected(false);
							return;  //update() is already called by ignoreAbsenceCheckBox
						}
						update();
					}
				}
			});
			
			
			update(); //initialize
			
			
			setBorder(new TitledBorder(new LineBorder(Color.BLACK), "Pairwise Network Comparison", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			
			GridBagLayout gridBagLayout = new GridBagLayout();
			gridBagLayout.columnWidths = new int[]{10, 25, 120, 30, 150, 10, 0};
			gridBagLayout.rowHeights = new int[]{10, 0, 0, 0, 0, 0, 0, 0, 10, 0};
			gridBagLayout.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
			gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
			setLayout(gridBagLayout);
			
			
			JLabel chooseNetworksLabel = new JLabel("Choose 2 networks to compare:");
			
			GridBagConstraints gbc_chooseNetworksLabel = new GridBagConstraints();
			gbc_chooseNetworksLabel.gridwidth = 2;
			gbc_chooseNetworksLabel.anchor = GridBagConstraints.NORTHWEST;
			gbc_chooseNetworksLabel.insets = new Insets(0, 0, 5, 5);
			gbc_chooseNetworksLabel.gridx = 1;
			gbc_chooseNetworksLabel.gridy = 1;
			add(chooseNetworksLabel, gbc_chooseNetworksLabel);
			
			
			GridBagConstraints gbc_highlightNodesCheckBox = new GridBagConstraints();
			gbc_highlightNodesCheckBox.anchor = GridBagConstraints.SOUTHWEST;
			gbc_highlightNodesCheckBox.insets = new Insets(0, 0, 5, 0);
			gbc_highlightNodesCheckBox.gridwidth = 2;
			gbc_highlightNodesCheckBox.gridx = 4;
			gbc_highlightNodesCheckBox.gridy = 1;
			add(highlightNodesCheckBox, gbc_highlightNodesCheckBox);
			
			greenBoxPanel = new JPanel();
			greenBoxPanel.setBackground(Color.GREEN);
			GridBagConstraints gbc_greenBoxPanel = new GridBagConstraints();
			gbc_greenBoxPanel.fill = GridBagConstraints.BOTH;
			gbc_greenBoxPanel.insets = new Insets(0, 0, 5, 5);
			gbc_greenBoxPanel.gridx = 1;
			gbc_greenBoxPanel.gridy = 2;
			add(greenBoxPanel, gbc_greenBoxPanel);
			
			
			GridBagConstraints gbc_networkAComboBox = new GridBagConstraints();
			gbc_networkAComboBox.fill = GridBagConstraints.HORIZONTAL;
			gbc_networkAComboBox.insets = new Insets(0, 0, 5, 5);
			gbc_networkAComboBox.gridx = 2;
			gbc_networkAComboBox.gridy = 2;
			add(networkAComboBox, gbc_networkAComboBox);
			
			nodePropertyLabel = new JLabel("Node property:");
			GridBagConstraints gbc_nodePropertyLabel = new GridBagConstraints();
			gbc_nodePropertyLabel.anchor = GridBagConstraints.SOUTHWEST;
			gbc_nodePropertyLabel.gridwidth = 2;
			gbc_nodePropertyLabel.insets = new Insets(0, 0, 5, 0);
			gbc_nodePropertyLabel.gridx = 4;
			gbc_nodePropertyLabel.gridy = 2;
			add(nodePropertyLabel, gbc_nodePropertyLabel);
			JLabel andLabel = new JLabel("and");
			
			GridBagConstraints gbc_andLabel = new GridBagConstraints();
			gbc_andLabel.insets = new Insets(0, 0, 5, 5);
			gbc_andLabel.gridx = 2;
			gbc_andLabel.gridy = 3;
			add(andLabel, gbc_andLabel);
			
			
			GridBagConstraints gbc_nodePropertyComboBox = new GridBagConstraints();
			gbc_nodePropertyComboBox.anchor = GridBagConstraints.NORTH;
			gbc_nodePropertyComboBox.insets = new Insets(0, 0, 5, 5);
			gbc_nodePropertyComboBox.fill = GridBagConstraints.HORIZONTAL;
			gbc_nodePropertyComboBox.gridx = 4;
			gbc_nodePropertyComboBox.gridy = 3;
			add(nodePropertyComboBox, gbc_nodePropertyComboBox);
			
			separator = new JSeparator();
			separator.setOrientation(SwingConstants.VERTICAL);
			GridBagConstraints gbc_separator = new GridBagConstraints();
			gbc_separator.fill = GridBagConstraints.VERTICAL;
			gbc_separator.gridheight = 7;
			gbc_separator.insets = new Insets(0, 0, 5, 5);
			gbc_separator.gridx = 3;
			gbc_separator.gridy = 1;
			add(separator, gbc_separator);
			
			redBoxPanel = new JPanel();
			redBoxPanel.setBackground(Color.RED);
			GridBagConstraints gbc_redBoxPanel = new GridBagConstraints();
			gbc_redBoxPanel.insets = new Insets(0, 0, 5, 5);
			gbc_redBoxPanel.fill = GridBagConstraints.BOTH;
			gbc_redBoxPanel.gridx = 1;
			gbc_redBoxPanel.gridy = 4;
			add(redBoxPanel, gbc_redBoxPanel);
			
			GridBagConstraints gbc_networkBComboBox = new GridBagConstraints();
			gbc_networkBComboBox.fill = GridBagConstraints.HORIZONTAL;
			gbc_networkBComboBox.insets = new Insets(0, 0, 5, 5);
			gbc_networkBComboBox.gridx = 2;
			gbc_networkBComboBox.gridy = 4;
			add(networkBComboBox, gbc_networkBComboBox);
			
			
			GridBagConstraints gbc_highlightEdgesCheckBox = new GridBagConstraints();
			gbc_highlightEdgesCheckBox.anchor = GridBagConstraints.SOUTHWEST;
			gbc_highlightEdgesCheckBox.insets = new Insets(0, 0, 5, 5);
			gbc_highlightEdgesCheckBox.gridx = 4;
			gbc_highlightEdgesCheckBox.gridy = 5;
			add(highlightEdgesCheckBox, gbc_highlightEdgesCheckBox);
			
			additionalOptionsLabel = new JLabel("Additional option:");
			GridBagConstraints gbc_additionalOptionsLabel = new GridBagConstraints();
			gbc_additionalOptionsLabel.anchor = GridBagConstraints.NORTHWEST;
			gbc_additionalOptionsLabel.gridwidth = 2;
			gbc_additionalOptionsLabel.insets = new Insets(0, 0, 5, 5);
			gbc_additionalOptionsLabel.gridx = 1;
			gbc_additionalOptionsLabel.gridy = 6;
			add(additionalOptionsLabel, gbc_additionalOptionsLabel);
			edgePropertyLabel = new JLabel("Edge property:");
			
			GridBagConstraints gbc_edgePropertyLabel = new GridBagConstraints();
			gbc_edgePropertyLabel.anchor = GridBagConstraints.SOUTHWEST;
			gbc_edgePropertyLabel.insets = new Insets(0, 0, 5, 5);
			gbc_edgePropertyLabel.gridx = 4;
			gbc_edgePropertyLabel.gridy = 6;
			add(edgePropertyLabel, gbc_edgePropertyLabel);
			
			
			GridBagConstraints gbc_ignoreAbsenceCheckBox = new GridBagConstraints();
			gbc_ignoreAbsenceCheckBox.anchor = GridBagConstraints.NORTHWEST;
			gbc_ignoreAbsenceCheckBox.gridwidth = 2;
			gbc_ignoreAbsenceCheckBox.insets = new Insets(0, 0, 5, 5);
			gbc_ignoreAbsenceCheckBox.gridx = 1;
			gbc_ignoreAbsenceCheckBox.gridy = 7;
			add(ignoreAbsenceCheckBox, gbc_ignoreAbsenceCheckBox);
			
			
			GridBagConstraints gbc_edgePropertyComboBox = new GridBagConstraints();
			gbc_edgePropertyComboBox.anchor = GridBagConstraints.NORTH;
			gbc_edgePropertyComboBox.fill = GridBagConstraints.HORIZONTAL;
			gbc_edgePropertyComboBox.insets = new Insets(0, 0, 5, 5);
			gbc_edgePropertyComboBox.gridx = 4;
			gbc_edgePropertyComboBox.gridy = 7;
			add(edgePropertyComboBox, gbc_edgePropertyComboBox);
			
		}
		
		@Override
		public void update(){		
			if (highlightNodesEnabled){
				appAdapter.getTaskManager().execute(new TaskIterator(
						new NodeChangeMapperTask(this, dynet, networkA, networkB, nodeProperty, ignoreAbsence)));
			}
			
			if (highlightEdgesEnabled){
				appAdapter.getTaskManager().execute(new TaskIterator(
						new EdgeChangeMapperTask(this, dynet, networkA, networkB, edgeProperty, ignoreAbsence)));
			}
		}

		@Override
		public void stopNodeColourControl() {
			highlightNodesCheckBox.setSelected(false);
		}

		@Override
		public void stopEdgeColourControl() {
			highlightEdgesCheckBox.setSelected(false);
		}

		@Override
		public JPanel getNodeInformationPanel(CyNode analyzedNode) {
			if (highlightNodesEnabled){
				return new NodeInformationPanel(dynet, networkA, networkB, nodeProperty, nodeDifferenceMap, analyzedNode);
			}else{
				return null;
			}
		}

		@Override
		public JPanel getEdgeInformationPanel(CyNode analyzedNode) {
			if (highlightEdgesEnabled){
				return new EdgeInformationPanel(dynet, networkA, networkB, edgeProperty, edgeDifferenceMap, analyzedNode);
			}else{
				return null;
			}
		}
		
		public void setEdgeMapperResult(Map<CyEdge, Double> edgeDifferenceMap){
			this.edgeDifferenceMap = edgeDifferenceMap;
			
			CyTable edgeTable = dynet.getUnionNetwork().getDefaultEdgeTable();
			edgeTable.deleteColumn(EDGE_RESULT_COLUMN_NAME);
			edgeTable.createColumn(EDGE_RESULT_COLUMN_NAME, Double.class, false);
			
			//result of the difference calculations is put on the edge table.
			for (CyEdge edge : edgeDifferenceMap.keySet()){
				edgeTable.getRow(edge.getSUID()).set(EDGE_RESULT_COLUMN_NAME, edgeDifferenceMap.get(edge));
			}
		}
		
		public void setNodeMapperResult(Map<CyNode, Double> nodeDifferenceMap){
			this.nodeDifferenceMap = nodeDifferenceMap;
			
			CyTable nodeTable = dynet.getUnionNetwork().getDefaultNodeTable();
			nodeTable.deleteColumn(NODE_RESULT_COLUMN_NAME);
			nodeTable.createColumn(NODE_RESULT_COLUMN_NAME, Double.class, false);
			
			//result of the difference calculations is put on the node table.
			for (CyNode node : nodeDifferenceMap.keySet()){
				nodeTable.getRow(node.getSUID()).set(NODE_RESULT_COLUMN_NAME, nodeDifferenceMap.get(node));
			}
		}
	}


