package eu.primes.dynet.internal.filter;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JComboBox;

/**
 * The interface for BooleanCyRowFilter. It only consists of a combobox with the options true and false.
 * 
 * @author Ivan Hendy Goenawan
 */
public class BooleanCyRowFilterPanel extends CyRowFilterPanel {

	private BooleanCyRowFilter rowFilter;
	private ActionListener listener;
	private JComboBox<String> trueFalseComboBox;
	
	public BooleanCyRowFilterPanel(BooleanCyRowFilter rowFilter, final ActionListener listener) {
		super(rowFilter, listener);
		this.rowFilter = rowFilter;
		this.listener = listener;
		
		trueFalseComboBox = new JComboBox<String>(new String[]{"true", "false"});
		if (rowFilter.getValue() == null){
			rowFilter.setValue(true);
			if (listener != null){
				listener.actionPerformed(new ActionEvent(BooleanCyRowFilterPanel.this, ActionEvent.ACTION_PERFORMED, null));
			}
		}
		if (rowFilter.getValue()){
			trueFalseComboBox.setSelectedIndex(0);
		}else{
			trueFalseComboBox.setSelectedIndex(1);
		}
		trueFalseComboBox.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED){
					int index = trueFalseComboBox.getSelectedIndex();
					if (index == 0){
						BooleanCyRowFilterPanel.this.rowFilter.setValue(true);
					}else{
						BooleanCyRowFilterPanel.this.rowFilter.setValue(false);
					}
					
					if (listener != null){
						listener.actionPerformed(new ActionEvent(BooleanCyRowFilterPanel.this, ActionEvent.ACTION_PERFORMED, null));
					}
				}
			}
		});
		
		
		
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0};
		gridBagLayout.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		
		GridBagConstraints gbc_trueFalseComboBox = new GridBagConstraints();
		gbc_trueFalseComboBox.fill = GridBagConstraints.VERTICAL;
		gbc_trueFalseComboBox.anchor = GridBagConstraints.WEST;
		gbc_trueFalseComboBox.gridx = 0;
		gbc_trueFalseComboBox.gridy = 0;
		add(trueFalseComboBox, gbc_trueFalseComboBox);
		
	}
	
	

}
