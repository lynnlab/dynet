package eu.primes.dynet.internal.filter;

import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyRow;

/**
 * The parent class for all CyRow filters. Child classes must implement the appropriate 'evaluate' attribute that returns either
 * true, false, or null for undefined result (such as when an edge filter is used for evaluating a node and vice versa). All values
 * in the CyRow for all columns in the ColumnGroup should be evaluated.
 * 
 * @author Ivan Hendy Goenawan
 */
public abstract class CyRowFilter {
	
	protected final ColumnGroup columnGroup;
	
	public CyRowFilter(ColumnGroup columnGroup){
		this.columnGroup = columnGroup;
	}
	
	
	/**
	 * If network is not given (null), the filter should evaluate all columns in the columngroup.
	 * Otherwise only evaluate columns specific to the network + columns not specific to any network
	 */
	public abstract Boolean evaluate (CyRow row, CyNetwork network);
	
	
	
	public ColumnGroup getColumnGroup(){
		return columnGroup;
	}
	
	/**
	 * Convenience method that automatically returns a child object of the correct type for the specified columnGroup.
	 */
	public static CyRowFilter createRowFilter(ColumnGroup columnGroup){
 		Class<?> type = columnGroup.getType();
		CyRowFilter newFilter;
		
 		if (type == Boolean.class){
			newFilter = new BooleanCyRowFilter(columnGroup);
		}else if (type == String.class){
			newFilter = new StringCyRowFilter(columnGroup);
		}else{
			newFilter = new NumericCyRowFilter(columnGroup);
		}
 		
 		return newFilter;
	}
	
	
}
