package eu.primes.dynet.internal.filter;

import javax.swing.tree.DefaultMutableTreeNode;

import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyRow;

/**
 * This is the nodes in the FilterTree (a JTree consists of nodes, don't confuse the term with nodes in a network). 
 * Each node can either be a leaf node which can contain an actual CyRowFilter, or they can be a non-leaf nodes which can 
 * contain one of four different join types (AND, OR, XOR, NOT).
 * 
 * If a node is a leaf, the evaluate method will just return the result from its CyRowFilter's evaluate method. Otherwise,
 * it will recursively evaluate the result from its child nodes and return the appropriate final result based on its join type.
 * 
 * Note that when a FilterNode return a null result (undefined result), it will be treated as if that particular filter node
 * just isn't there. So, a newly created filter node will not affect filtering results. Likewise, nodes that have been set to
 * filter based on an edge attribute won't have any effect when the entire FilterTree is used for evaluating a node (and vice versa).
 * 
 * @author Ivan Hendy Goenawan
 */
public class FilterNode extends DefaultMutableTreeNode{
	
	private JoinType joinType;
	private CyRowFilter rowFilter;
	
	//remember the NOT join type can only have a maximum of 1 child
	public enum JoinType{
		AND, OR, XOR, NOT
	}
	
	
	public FilterNode(){
	}
	
	public JoinType getJoinType(){
		return joinType;
	}
	
	public void setJoinType(JoinType joinType){
		this.joinType = joinType;
	}
	
	public CyRowFilter getRowFilter(){
		return rowFilter;
	}
	
	public void setRowFilter(CyRowFilter rowFilter){
		this.rowFilter = rowFilter;
	}
	
	public Boolean evaluate (CyRow row, CyNetwork network){
		if (isLeaf()){
			if (isRoot()) return true;
			else if (rowFilter == null) return null;       //return null for undefined result when a node is newly created (no rowFilter set)
			else return rowFilter.evaluate(row, network);
		}else{
			int trueCount = 0;
			int falseCount = 0;
			
			for (int i = 0; i < children.size(); i++){
				Boolean tempResult = ((FilterNode)children.get(i)).evaluate(row, network);
				if (tempResult != null){                //children who returns null value (undefined result) will be ignored
					if (tempResult) trueCount++;
					else falseCount++;
				}
			}
			
			if (trueCount == 0 && falseCount == 0){
				if (isRoot()) return true;
				else return null;
			}

			switch(joinType){
			case AND:
				return (falseCount == 0) ? true : false;
			case OR:
				return (trueCount >= 1) ? true : false;
			case XOR:
				return (trueCount == 1) ? true : false;
			case NOT:
				return (trueCount == 1) ? false : true;    //assumes that there must be only one child
			default:
				return null;
			}
		}
	}
}
