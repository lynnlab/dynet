package eu.primes.dynet.internal.filter;

import java.util.List;

import org.cytoscape.work.undo.AbstractCyEdit;

/**
 * A class for grouping many CyEdits into one 
 * 
 * @author Ivan Hendy Goenawan
 */
public class EditGroup extends AbstractCyEdit {

	private List<AbstractCyEdit> edits;
	
	public EditGroup(String description, List<AbstractCyEdit> edits){
		super(description);
		this.edits = edits;
	}
	
	@Override
	public void undo() {
		if (edits == null) return;
		
		for (int i = edits.size() - 1; i >= 0; i--){
			edits.get(i).undo();
		}
	}

	@Override
	public void redo() {
		if (edits == null) return;
		
		for (int i = 0; i < edits.size(); i++){
			edits.get(i).redo();
		}
	}
}
