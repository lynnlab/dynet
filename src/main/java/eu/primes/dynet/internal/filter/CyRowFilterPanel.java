package eu.primes.dynet.internal.filter;

import java.awt.event.ActionListener;

import javax.swing.JPanel;

/**
 * The parent class for all CyRowFilterPanels, which are the interface for different types of filter. This class also
 * provides a convenience method that returns a child object of the appropriate type for the specified CyRowFilter.
 * 
 * @author Ivan Hendy Goenawan
 */
public abstract class CyRowFilterPanel extends JPanel {
	
	public CyRowFilterPanel(CyRowFilter rowFilter, ActionListener listener) {
	}
	
	
	public static CyRowFilterPanel createCyRowFilterPanel(CyRowFilter rowFilter, ActionListener listener){
		if (rowFilter instanceof StringCyRowFilter){
			return new StringCyRowFilterPanel((StringCyRowFilter)rowFilter, listener);
		}else if (rowFilter instanceof BooleanCyRowFilter){
			return new BooleanCyRowFilterPanel((BooleanCyRowFilter)rowFilter, listener);
		}else if (rowFilter instanceof NumericCyRowFilter){
			return new NumericCyRowFilterPanel((NumericCyRowFilter)rowFilter, listener);
		}else{
			return null;
		}
	}
}
