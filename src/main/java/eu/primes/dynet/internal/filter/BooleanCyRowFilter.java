package eu.primes.dynet.internal.filter;

import java.util.Set;

import org.cytoscape.model.CyColumn;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyRow;

/**
 * A filter that can evaluate a CyRow based on a boolean attribute.
 * 
 * @author Ivan Hendy Goenawan
 */
public class BooleanCyRowFilter extends CyRowFilter {

	private Boolean value;
	
	public BooleanCyRowFilter(ColumnGroup columnGroup) {
		super(columnGroup);
	}

	/**
	 * Evaluate whether or not a CyRow matches the specified boolean value.
	 * 
	 * If no column has been set, return null.
	 * If the column table is not the same as the evaluated row's table (e.g. when an edge filter is applied to a node), also return null.
	 * If either one of the filter's value or the actual row's value is null, return false (considered as not matching).
	 *
	 * If network is not given (null), the filter should evaluate all columns in the columngroup.
	 * Otherwise only evaluate columns specific to the network + columns not specific to any network.
	 */
	@Override
	public Boolean evaluate(CyRow row, CyNetwork network) {
		if (columnGroup == null) return null;
		if (columnGroup.getTable() != row.getTable()) return null;
		if (value == null) return false;
		
		Boolean result = null;
		
		for (CyColumn column : columnGroup.getColumns(network)){
			Boolean rowValue = row.get(column.getName(), Boolean.class);
			
			boolean tempResult;
			if (rowValue == null){
				tempResult = false;
			}else{
				tempResult =  (rowValue == value);
			}
			
			if (columnGroup.isJoinTypeAnd()){
				if (result == null) result = tempResult;
				else result &= tempResult;
				if (!result) break;
			}else{
				if (result == null) result = tempResult;
				else result |= tempResult;
				if (result) break;
			}
		}
		
		return result;
	}
	
	public Boolean getValue(){
		return value;
	}
	
	public void setValue(Boolean value){
		this.value = value;
	}
}
