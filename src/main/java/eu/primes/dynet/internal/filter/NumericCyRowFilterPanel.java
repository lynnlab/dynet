package eu.primes.dynet.internal.filter;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cytoscape.model.CyColumn;

import eu.primes.dynet.internal.rangeslider.RangeSlider;

/**
 * The interface for NumericCyRowFilter. It consists of two textboxes for specifying the lower and upper bounds, a checkbox for specifying
 * whether or not the range is inclusive, and a slider with two knobs to easily adjust the range.
 * 
 * @author Ivan Hendy Goenawan
 */
public class NumericCyRowFilterPanel extends CyRowFilterPanel {

	private NumericCyRowFilter rowFilter;
	private ActionListener listener;

	private JTextField lowValueTextField;
	private JTextField highValueTextField;
	private JCheckBox inclusiveCheckBox;
	private RangeSlider rangeSlider;
	
	private Object minValue;
	private Object maxValue;
	private Object nonInfiniteMinValue;
	private Object nonInfiniteMaxValue;
	
	
	private boolean needToUpdate;
	private boolean modifyingValues;
	private JLabel isBetweenLabel;
	private JLabel andLabel;
	
	
	public NumericCyRowFilterPanel(NumericCyRowFilter rowFilter, final ActionListener listener) {
		super(rowFilter, listener);
		this.rowFilter = rowFilter;
		this.listener = listener;
		
		findMinMax(rowFilter.getColumnGroup());
		
		Class<?> type =  rowFilter.getColumnGroup().getType();
		
		if (minValue == null){
			if (type == Integer.class) minValue = new Integer(0);
			else if (type == Long.class) minValue = new Long(0);
			else if (type == Double.class) minValue = new Double(0);
		}else if (type == Double.class){
			if (isFinite((Double)minValue)){
				minValue = new BigDecimal((Double)minValue).setScale(3, RoundingMode.FLOOR).doubleValue();  //round to 3 decimal places
			}
			
			if (nonInfiniteMinValue != null){
				nonInfiniteMinValue = new BigDecimal((Double)nonInfiniteMinValue).setScale(3, RoundingMode.FLOOR).doubleValue();
			}
		}
		
		if (maxValue == null){
			if (type == Integer.class) maxValue = new Integer(0);
			else if (type == Long.class) maxValue = new Long(0);
			else if (type == Double.class) maxValue = new Double(0);
		}else if (type == Double.class){
			if (isFinite((Double)maxValue)){
				maxValue = new BigDecimal((Double)maxValue).setScale(3, RoundingMode.CEILING).doubleValue(); //round to 3 decimal places
			}
			
			if (nonInfiniteMaxValue != null){
				nonInfiniteMaxValue = new BigDecimal((Double)nonInfiniteMaxValue).setScale(3, RoundingMode.CEILING).doubleValue();
			}
		}
		
		needToUpdate = false;
		
		lowValueTextField = new JTextField();
		lowValueTextField.setColumns(5);
		if (rowFilter.getLowValue() == null){
			rowFilter.setLowValue(minValue);
			needToUpdate = true;
		}
		lowValueTextField.setText(rowFilter.getLowValue().toString());
		lowValueTextField.addFocusListener(new FocusListener() {
			@Override
			public void focusLost(FocusEvent e) {	
			}
			@Override
			public void focusGained(FocusEvent e) {
				lowValueTextField.selectAll();
			}
		});
		
		lowValueTextField.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void removeUpdate(DocumentEvent e) {
				update();
			}
			@Override
			public void insertUpdate(DocumentEvent e) {
				update();
			}
			@Override
			public void changedUpdate(DocumentEvent e) {
				update();
			}
			public void update(){
				if (!modifyingValues){
					modifyingValues = true;
					
					String lowValue = lowValueTextField.getText();
					Class<?> type = NumericCyRowFilterPanel.this.rowFilter.getColumnGroup().getType();
					
					try{
						if (type == Integer.class) NumericCyRowFilterPanel.this.rowFilter.setLowValue(Integer.parseInt(lowValue));
						else if (type == Long.class) NumericCyRowFilterPanel.this.rowFilter.setLowValue(Long.parseLong(lowValue));
						else if (type == Double.class) NumericCyRowFilterPanel.this.rowFilter.setLowValue(Double.parseDouble(lowValue));
					}catch (Exception exception){
					}
					
					rangeSlider.setValue(convertToStep(NumericCyRowFilterPanel.this.rowFilter.getLowValue()));
					
					if (listener != null){
						listener.actionPerformed(new ActionEvent(NumericCyRowFilterPanel.this, ActionEvent.ACTION_PERFORMED, null));
					}
					
					modifyingValues = false;
				}
			}
		});
		
		
		
		highValueTextField = new JTextField();
		highValueTextField.setColumns(5);
		if (rowFilter.getHighValue() == null){
			rowFilter.setHighValue(maxValue);
			needToUpdate = true;
		}
		highValueTextField.setText(rowFilter.getHighValue().toString());
		highValueTextField.addFocusListener(new FocusListener() {
			@Override
			public void focusLost(FocusEvent e) {	
			}
			@Override
			public void focusGained(FocusEvent e) {
				highValueTextField.selectAll();
			}
		});
		
		highValueTextField.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void removeUpdate(DocumentEvent e) {
				update();
			}
			@Override
			public void insertUpdate(DocumentEvent e) {
				update();
			}
			@Override
			public void changedUpdate(DocumentEvent e) {
				update();
			}
			public void update(){
				if (!modifyingValues){
					modifyingValues = true;
					
					String highValue = highValueTextField.getText();
					Class<?> type = NumericCyRowFilterPanel.this.rowFilter.getColumnGroup().getType();
					
					try{
						if (type == Integer.class) NumericCyRowFilterPanel.this.rowFilter.setHighValue(Integer.parseInt(highValue));
						else if (type == Long.class) NumericCyRowFilterPanel.this.rowFilter.setHighValue(Long.parseLong(highValue));
						else if (type == Double.class) NumericCyRowFilterPanel.this.rowFilter.setHighValue(Double.parseDouble(highValue));
					}catch (Exception exception){
					}
					
					rangeSlider.setUpperValue(convertToStep(NumericCyRowFilterPanel.this.rowFilter.getHighValue()));
					
					if (listener != null){
						listener.actionPerformed(new ActionEvent(NumericCyRowFilterPanel.this, ActionEvent.ACTION_PERFORMED, null));
					}
					
					modifyingValues = false;
				}
			}
		});
		
		
		//when the RangeSlider is first created, both sliders I think is at 500. If the lower value is more than 500, be careful
		//not to try to move the lower left slider to the right first, because it won't be able to move past the right slider.
		rangeSlider = new RangeSlider(0, 1000);        //the min and max step should probably be made into constant variables
		int lowPosition = convertToStep(rowFilter.getLowValue());
		int highPosition = convertToStep(rowFilter.getHighValue());
		if (lowPosition > 500){
			rangeSlider.setUpperValue(highPosition);
			rangeSlider.setValue(lowPosition);
		}else{
			rangeSlider.setValue(lowPosition);
			rangeSlider.setUpperValue(highPosition);
		}
		rangeSlider.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				if (!modifyingValues){
					modifyingValues = true;
					
					RangeSlider rangeSlider = (RangeSlider)e.getSource();
					Object lowValue = convertToValue(rangeSlider.getValue());
					Object highValue = convertToValue(rangeSlider.getUpperValue());
					
					NumericCyRowFilterPanel.this.rowFilter.setLowValue(lowValue);
					NumericCyRowFilterPanel.this.rowFilter.setHighValue(highValue);
					
					NumericCyRowFilterPanel.this.lowValueTextField.setText(lowValue.toString());
					NumericCyRowFilterPanel.this.highValueTextField.setText(highValue.toString());
				
					if (listener != null){
						listener.actionPerformed(new ActionEvent(NumericCyRowFilterPanel.this, ActionEvent.ACTION_PERFORMED, null));
					}
					
					modifyingValues = false;
				}
			}
		});
		
		
		
		inclusiveCheckBox = new JCheckBox("Inclusive");
		if (rowFilter.getInclusive()) inclusiveCheckBox.setSelected(true);
		inclusiveCheckBox.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED){
					NumericCyRowFilterPanel.this.rowFilter.setInclusive(true);
				}else if (e.getStateChange() == ItemEvent.DESELECTED){
					NumericCyRowFilterPanel.this.rowFilter.setInclusive(false);
				}
				
				if (listener != null){
					listener.actionPerformed(new ActionEvent(NumericCyRowFilterPanel.this, ActionEvent.ACTION_PERFORMED, null));
				}
			}
		});
		
		
		
		if (needToUpdate && listener != null){
			listener.actionPerformed(new ActionEvent(NumericCyRowFilterPanel.this, ActionEvent.ACTION_PERFORMED, null));
		}
		
		
		
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{50, 50, 0, 50, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		GridBagConstraints gbc_rangeSlider = new GridBagConstraints();
		gbc_rangeSlider.fill = GridBagConstraints.BOTH;
		gbc_rangeSlider.gridx = 0;
		gbc_rangeSlider.gridy = 1;
		gbc_rangeSlider.gridwidth = 5;
		add(rangeSlider, gbc_rangeSlider);
		
		
		isBetweenLabel = new JLabel("is between");
		GridBagConstraints gbc_isBetweenLabel = new GridBagConstraints();
		gbc_isBetweenLabel.insets = new Insets(0, 0, 5, 5);
		gbc_isBetweenLabel.anchor = GridBagConstraints.WEST;
		gbc_isBetweenLabel.gridx = 0;
		gbc_isBetweenLabel.gridy = 0;
		add(isBetweenLabel, gbc_isBetweenLabel);
		
		
		GridBagConstraints gbc_lowValueTextField = new GridBagConstraints();
		gbc_lowValueTextField.insets = new Insets(0, 0, 5, 5);
		gbc_lowValueTextField.fill = GridBagConstraints.BOTH;
		gbc_lowValueTextField.gridx = 1;
		gbc_lowValueTextField.gridy = 0;
		add(lowValueTextField, gbc_lowValueTextField);
		
		
		andLabel = new JLabel("and");
		GridBagConstraints gbc_andLabel = new GridBagConstraints();
		gbc_andLabel.insets = new Insets(0, 0, 5, 5);
		gbc_andLabel.anchor = GridBagConstraints.EAST;
		gbc_andLabel.gridx = 2;
		gbc_andLabel.gridy = 0;
		add(andLabel, gbc_andLabel);
		
		GridBagConstraints gbc_highValueTextField = new GridBagConstraints();
		gbc_highValueTextField.insets = new Insets(0, 0, 5, 5);
		gbc_highValueTextField.fill = GridBagConstraints.BOTH;
		gbc_highValueTextField.gridx = 3;
		gbc_highValueTextField.gridy = 0;
		add(highValueTextField, gbc_highValueTextField);
		
		GridBagConstraints gbc_inclusiveCheckBox = new GridBagConstraints();
		gbc_inclusiveCheckBox.insets = new Insets(0, 0, 5, 0);
		gbc_inclusiveCheckBox.fill = GridBagConstraints.BOTH;
		gbc_inclusiveCheckBox.gridx = 4;
		gbc_inclusiveCheckBox.gridy = 0;
		add(inclusiveCheckBox, gbc_inclusiveCheckBox);
	
	}

	/**
	 * Find the minimal and maximal values in all columns in the ColumnGroup
	 */
	private void findMinMax(ColumnGroup columnGroup){
		minValue = maxValue = nonInfiniteMinValue = nonInfiniteMaxValue = null;
		
		ArrayList<Object> values = new ArrayList<Object>();
		for (CyColumn column : columnGroup.getColumns(null)){
			values.addAll(column.getValues(columnGroup.getType()));
		}
		
		for (Object value : values){
			if (nonInfiniteMaxValue == null || ((Comparable)value).compareTo(nonInfiniteMaxValue) > 0){
				if (value.getClass() == Double.class){
					if (isFinite((Double)value)) nonInfiniteMaxValue = value;
					if (maxValue == null || (Double)maxValue != Double.POSITIVE_INFINITY) maxValue = value;
				}else{
					nonInfiniteMaxValue = value;
					maxValue = value;
				}
			}
			if (nonInfiniteMinValue == null || ((Comparable)value).compareTo(nonInfiniteMinValue) < 0){
				if (value.getClass() == Double.class){
					if (isFinite((Double)value)) nonInfiniteMinValue = value;
					if (minValue == null || (Double)minValue != Double.NEGATIVE_INFINITY) minValue = value;
				}else{
					nonInfiniteMinValue = value;
					minValue = value;
				}
			}
		}
	}
	
	/**
	 * Convert a value to the number of step in the slider
	 * It assumes that the min and max step of the slider are 0 and 1000 respectively.
	 */
	private int convertToStep(Object value){
		if (((Comparable)value).compareTo(minValue) <= 0) return 0;
		if (((Comparable)value).compareTo(maxValue) >= 0) return 1000;
		
		Class<?> type = rowFilter.getColumnGroup().getType();
		
		if (type == Integer.class){
			int range = (Integer)maxValue - (Integer)minValue;
			int distanceFromMin = (Integer)value - (Integer)minValue;
			return (int)Math.round(((double)distanceFromMin / range) * 1000);
		}
		else if (type == Long.class){
			long range = (Long)maxValue - (Long)minValue;
			long distanceFromMin = (Long)value - (Long)minValue;
			return (int)Math.round(((double)distanceFromMin / range) * 1000);
		}
		else if (type == Double.class){	
			if (nonInfiniteMaxValue != null){       //also means nonInfiniteMinValue != null
				if (((Comparable)value).compareTo(nonInfiniteMinValue) <= 0) return 1;
				if (((Comparable)value).compareTo(nonInfiniteMaxValue) >= 0) return 999;
				
				//if range is 0, then one of the two if conditions above must have been correct
				double range = (Double)nonInfiniteMaxValue - (Double)nonInfiniteMinValue;
				double distanceFromMin = (Double)value - (Double)nonInfiniteMinValue;
				return 1 + (int) Math.round((distanceFromMin / range) * 998);
			}else{
				return 500;
			}
		}
		
		return 0;
	}
	
	/**
	 * Convert the number of step in the slider to the real value, rounded to 3 decimal places
	 * It assumes that the min and max step of the slider are 0 and 1000 respectively.
	 */
	private Object convertToValue(int step){
		Class<?> type = rowFilter.getColumnGroup().getType();
		
		if (type == Integer.class){
			int range = (Integer)maxValue - (Integer)minValue;
			return (int)Math.round((Integer)minValue + ((double)step / 1000) * range); 
		}else if (type == Long.class){
			long range = (Long)maxValue - (Long)minValue;
			return (long)Math.round((Long)minValue + ((double)step / 1000) * range);
		}else if (type == Double.class){
			if (step == 0) return minValue;
			if (step == 1000) return maxValue;
			
			double value;
			
			if (nonInfiniteMaxValue != null){       //also means nonInfiniteMinValue != null
				double range = (Double)nonInfiniteMaxValue - (Double)nonInfiniteMinValue;
				value = (Double)nonInfiniteMinValue + ((double)(step-1) / 998) * range;
				value = new BigDecimal(value).setScale(3, RoundingMode.HALF_UP).doubleValue();
			}else{
				if (step < 500) value = (Double)minValue;
				else value = (Double)maxValue;
			}
			
			return value;
		}
		
		return null;
	}
	
	
	//because isFinite only exists starting from Java 8
	private static boolean isFinite(double number){
		return !Double.isInfinite(number) && !Double.isNaN(number);
	}
}
