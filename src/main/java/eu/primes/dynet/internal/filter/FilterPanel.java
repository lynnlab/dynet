package eu.primes.dynet.internal.filter;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.tree.DefaultTreeModel;

import org.cytoscape.app.CyAppAdapter;
import org.cytoscape.model.CyEdge;
import org.cytoscape.model.CyEdge.Type;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;
import org.cytoscape.model.CyRow;
import org.cytoscape.model.events.ColumnCreatedEvent;
import org.cytoscape.model.events.ColumnCreatedListener;
import org.cytoscape.model.events.ColumnDeletedEvent;
import org.cytoscape.model.events.ColumnDeletedListener;
import org.cytoscape.model.subnetwork.CySubNetwork;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.model.View;
import org.cytoscape.view.presentation.property.BasicVisualLexicon;
import org.cytoscape.work.undo.AbstractCyEdit;

import eu.primes.dynet.internal.ControlPanel;
import eu.primes.dynet.internal.ControlPanel.ControlPanelComponent;
import eu.primes.dynet.internal.DynamicNetwork;
import eu.primes.dynet.internal.DynamicNetwork.CustomEdge;
import eu.primes.dynet.internal.DynamicNetwork.CustomNode;

/**
 * A control panel component that provides the interface for the filtering system. The filters are stored in a tree structure in the
 * filterTree variable.
 * 
 * @author Ivan Hendy Goenawan
 */
public class FilterPanel extends ControlPanelComponent implements ColumnCreatedListener, ColumnDeletedListener {
	private ControlPanel controlPanel;
	private DynamicNetwork dynet;
	private CyAppAdapter appAdapter;
	private boolean enabled;
	private boolean edgelessNodeVisible;
	
	private FilterTree filterTree;
	private JCheckBox onOffCheckBox;
	private JCheckBox edgelessNodeCheckBox;
	private JButton deleteComponentsButton;
	
	public FilterPanel(ControlPanel controlPanel, DynamicNetwork dynet, CyAppAdapter appAdapter){
		this.controlPanel = controlPanel;
		this.dynet = dynet;
		this.appAdapter = appAdapter;
		
		//listens to any changes in the filtering settings and then reapply the filters by calling the update method
		ActionListener treeListener = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				update();
			}
		};
		
		filterTree = new FilterTree(dynet, treeListener);
		
		//checkbox for filtering out nodes that have no edges. This is useful in cases where we are filtering by edges and we want to
		//leave out nodes that have no edges left
		edgelessNodeCheckBox = new JCheckBox("Hide edgeless nodes");
		edgelessNodeCheckBox.setSelected(false);
		edgelessNodeVisible = true;
		edgelessNodeCheckBox.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED){
					edgelessNodeVisible = false;
				}else if (e.getStateChange() == ItemEvent.DESELECTED){
					edgelessNodeVisible = true;
				}
				update();
			}
		});
		
		onOffCheckBox = new JCheckBox("Turn on filtering");
		onOffCheckBox.addItemListener(new ItemListener() {	
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED){
					enabled = true;
				}else if (e.getStateChange() == ItemEvent.DESELECTED){
					enabled = false;
				}
				update();
			}
		});
		onOffCheckBox.setSelected(true);
		
		//Button for deleting permanently nodes and edges that are currently filtered out (it can be undone). This is useful when 
		//we want to exclude certain nodes and edges from subsequent analysis
		deleteComponentsButton = new JButton("Delete Filtered Components");
		deleteComponentsButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int result = JOptionPane.showConfirmDialog(null, "Delete all filtered nodes and edges?", "Dynet Filter", JOptionPane.OK_CANCEL_OPTION);
				if (result == JOptionPane.OK_OPTION){
					deleteAllHidden(FilterPanel.this.dynet);
				}
			}
		});
		
		
		
		setBorder(new TitledBorder(new LineBorder(Color.BLACK), "Filter", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{10, 150, 150, 40, 10, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 200, 10, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		GridBagConstraints gbc_onOffCheckBox = new GridBagConstraints();
		gbc_onOffCheckBox.anchor = GridBagConstraints.NORTHWEST;
		gbc_onOffCheckBox.insets = new Insets(0, 0, 5, 5);
		gbc_onOffCheckBox.gridx = 1;
		gbc_onOffCheckBox.gridy = 0;
		add(onOffCheckBox, gbc_onOffCheckBox);
		
		GridBagConstraints gbc_deleteComponentsButton = new GridBagConstraints();
		gbc_deleteComponentsButton.insets = new Insets(0, 0, 5, 5);
		gbc_deleteComponentsButton.gridx = 2;
		gbc_deleteComponentsButton.gridy = 0;
		add(deleteComponentsButton, gbc_deleteComponentsButton);
		
		GridBagConstraints gbc_edgelessNodeCheckBox = new GridBagConstraints();
		gbc_edgelessNodeCheckBox.insets = new Insets(0, 0, 5, 5);
		gbc_edgelessNodeCheckBox.anchor = GridBagConstraints.NORTHWEST;
		gbc_edgelessNodeCheckBox.gridx = 1;
		gbc_edgelessNodeCheckBox.gridy = 1;
		add(edgelessNodeCheckBox, gbc_edgelessNodeCheckBox);
		
		
		GridBagConstraints gbc_filterTree = new GridBagConstraints();
		gbc_filterTree.gridwidth = 3;
		gbc_filterTree.insets = new Insets(0, 0, 5, 5);
		gbc_filterTree.fill = GridBagConstraints.BOTH;
		gbc_filterTree.gridx = 1;
		gbc_filterTree.gridy = 2;
		add(filterTree, gbc_filterTree);
		
	}

	@Override
	public void update() {
		if (enabled){
			applyFilter(dynet, (FilterNode)filterTree.getModel().getRoot());
			if (!edgelessNodeVisible) filterEdgelessNodes(dynet);
		}else{
			resetFilter(dynet);
		}
		
		dynet.getUnionNetworkView().updateView();
		for (CyNetworkView networkView : dynet.getMemberNetworkViews()){
			networkView.updateView();
		}
	}
	
	/**
	 * Apply the filters to all nodes and edges one by one. We only need to call evaluate on the root of the filter tree because 
	 * it will then recursively evaluate all its children. Each member network is filtered independently.
	 */
	public void applyFilter(DynamicNetwork dynet, FilterNode rootFilter){
		CyNetwork unionNetwork = dynet.getUnionNetwork();
		CyNetworkView unionNetworkView = dynet.getUnionNetworkView();
		
		List<CyNode> nodes = unionNetwork.getNodeList();
		List<CyEdge> edges = unionNetwork.getEdgeList();
		
		/*This first loop is needed to prevent bug if an edge is made visible when its
		 * nodes are hidden.
		 */
		for(CyNode node : nodes){
			unionNetworkView.getNodeView(node).setLockedValue(BasicVisualLexicon.NODE_VISIBLE, true);
			
			for (CustomNode correspondingNode : dynet.getCorrespondingNodes(node, unionNetwork)){
				View<CyNode> correspondingNodeView = correspondingNode.getNodeView();
				if (correspondingNodeView != null){
					correspondingNodeView.setLockedValue(BasicVisualLexicon.NODE_VISIBLE, true);
				}
			}
		}
		
		
		for (CyEdge edge : edges){
			CyRow row = unionNetwork.getRow(edge);

			boolean result = rootFilter.evaluate(row, null); //the union network is evaluated using all columns related to all networks.
			unionNetworkView.getEdgeView(edge).setLockedValue(BasicVisualLexicon.EDGE_VISIBLE, result);
			
			for (CustomEdge correspondingEdge : dynet.getCorrespondingEdges(edge, unionNetwork)){
				View<CyEdge> correspondingEdgeView = correspondingEdge.getEdgeView();
				if (correspondingEdgeView != null){
					boolean correspondingResult = rootFilter.evaluate(row, correspondingEdge.getNetwork());   //the member network is evaluated using only columns that belong specifically to it.
					correspondingEdgeView.setLockedValue(BasicVisualLexicon.EDGE_VISIBLE, correspondingResult);
				}
				
			}
		}
		
		for (CyNode node : nodes){
			CyRow row = unionNetwork.getRow(node);
			
			boolean result = rootFilter.evaluate(row, null);  //the union network is evaluated using all columns related to all networks.
			unionNetworkView.getNodeView(node).setLockedValue(BasicVisualLexicon.NODE_VISIBLE, result);
			if (!result){
				List<CyEdge> adjacentEdges = unionNetwork.getAdjacentEdgeList(node, Type.ANY);
				for (CyEdge adjacentEdge : adjacentEdges){
					unionNetworkView.getEdgeView(adjacentEdge).setLockedValue(BasicVisualLexicon.EDGE_VISIBLE, false);
				}
			}
			
			
			for (CustomNode correspondingNode : dynet.getCorrespondingNodes(node, unionNetwork)){
				View<CyNode> correspondingNodeView = correspondingNode.getNodeView();
				if (correspondingNodeView != null){
					boolean correspondingResult = rootFilter.evaluate(row, correspondingNode.getNetwork());   //the member network is evaluated using only columns that belong specifically to it.
					correspondingNodeView.setLockedValue(BasicVisualLexicon.NODE_VISIBLE, correspondingResult);
					if (!correspondingResult){
						List<CyEdge> adjacentEdges = correspondingNode.getNetwork().getAdjacentEdgeList(correspondingNode.getNode(), Type.ANY);
						for (CyEdge adjacentEdge : adjacentEdges){
							correspondingNode.getNetworkView().getEdgeView(adjacentEdge).setLockedValue(BasicVisualLexicon.EDGE_VISIBLE, false);
						}
					}
				}
			}
		}
		
	}
	
	/**
	 * Method for filtering out nodes that have no edges. This is useful in situations where the user is filtering based on edges and
	 * want to leave out nodes that are left without any edges.
	 */
	public void filterEdgelessNodes(DynamicNetwork dynet){
		ArrayList<CyNetwork> allNetworks = new ArrayList<CyNetwork>();
		allNetworks.add(dynet.getUnionNetwork());
		allNetworks.addAll(dynet.getMemberNetworks());
		
		for (CyNetwork network : allNetworks){
			if (!appAdapter.getCyNetworkViewManager().getNetworkViews(network).isEmpty()){
				CyNetworkView networkView = appAdapter.getCyNetworkViewManager().getNetworkViews(network).iterator().next();
				
				for (CyNode node : network.getNodeList()){
					boolean isolated = true;
					List<CyEdge> adjacentEdges = network.getAdjacentEdgeList(node, Type.ANY);
					
					for (CyEdge adjacentEdge : adjacentEdges){
						View<CyEdge> adjacentEdgeView = networkView.getEdgeView(adjacentEdge);
						
						if (adjacentEdgeView.getVisualProperty(BasicVisualLexicon.EDGE_VISIBLE)){
							isolated = false;
							break;
						}
					}
					
					if (isolated){
						networkView.getNodeView(node).setLockedValue(BasicVisualLexicon.NODE_VISIBLE, false);
					}
				}
			}
		}
	}
	
	/**
	 * Make everything visible again
	 */
	public void resetFilter(DynamicNetwork dynet){
		ArrayList<CyNetwork> allNetworks = new ArrayList<CyNetwork>();
		allNetworks.add(dynet.getUnionNetwork());
		allNetworks.addAll(dynet.getMemberNetworks());
		
		for (CyNetwork network : allNetworks){
			if (!appAdapter.getCyNetworkViewManager().getNetworkViews(network).isEmpty()){
				CyNetworkView networkView = appAdapter.getCyNetworkViewManager().getNetworkViews(network).iterator().next();
				
				Collection<View<CyNode>> nodeViews = networkView.getNodeViews();
				Collection<View<CyEdge>> edgeViews = networkView.getEdgeViews();
				
				for (View<CyNode> nodeView : nodeViews){
					nodeView.clearValueLock(BasicVisualLexicon.NODE_VISIBLE);
				}
				
				for (View<CyEdge> edgeView : edgeViews){
					edgeView.clearValueLock(BasicVisualLexicon.EDGE_VISIBLE);
				}
				
				/*Note: if an edge is set to be visible when one of its nodes is not visible, cytoscape will produce
				 * annoying bug. So we must remember to make visible all the nodes first.
				 */
			}
		}
	}
	
	/**
	 * Delete permanently nodes and edges that are currently filtered out (it can be undone). This is useful when 
	 * we want to exclude certain nodes and edges from subsequent analysis. Nodes and edges that are filtered
	 * in the union network along with all their corresponding nodes and edges will be deleted even if those
	 * corresponding nodes and edges are not filtered out in their own network. We have to do this because
	 * the filter only evaluates data from the union network node/edge table. So if a node/edge is deleted in
	 * the union network, the filter can no longer evaluate it.
	 * 
	 */
	public void deleteAllHidden(DynamicNetwork dynet){
		CyNetwork unionNetwork = dynet.getUnionNetwork();
		CyNetworkView unionNetworkView = dynet.getUnionNetworkView();
		
		HashMap<CyNetwork, List<CyNode>> hiddenNodes = new HashMap<CyNetwork, List<CyNode>>();
		HashMap<CyNetwork, List<CyEdge>> hiddenEdges = new HashMap<CyNetwork, List<CyEdge>>();

		hiddenNodes.put(unionNetwork, new ArrayList<CyNode>());
		hiddenEdges.put(unionNetwork, new ArrayList<CyEdge>());
		for (CyNetwork memberNetwork : dynet.getMemberNetworks()){
			hiddenNodes.put(memberNetwork, new ArrayList<CyNode>());
			hiddenEdges.put(memberNetwork, new ArrayList<CyEdge>());
		}
		
		//find all hidden nodes in the union network and their corresponding nodes in other networks
		for (View<CyNode> nodeView : unionNetworkView.getNodeViews()){
			if (nodeView.getVisualProperty(BasicVisualLexicon.NODE_VISIBLE) == false){
				hiddenNodes.get(unionNetwork).add(nodeView.getModel());
				
				for (CustomNode correspondingNode : dynet.getCorrespondingNodes(nodeView.getModel(), unionNetwork)){
					CyNetwork memberNetwork = correspondingNode.getNetwork();
					if (memberNetwork != null){
						hiddenNodes.get(memberNetwork).add(correspondingNode.getNode());
					}
				}
			}
		}
		
		//find all hidden edges in the union network and their corresponding edges in other networks
		for (View<CyEdge> edgeView : unionNetworkView.getEdgeViews()){
			if (edgeView.getVisualProperty(BasicVisualLexicon.EDGE_VISIBLE) == false){
				hiddenEdges.get(unionNetwork).add(edgeView.getModel());
				
				for (CustomEdge correspondingEdge : dynet.getCorrespondingEdges(edgeView.getModel(), unionNetwork)){
					CyNetwork memberNetwork = correspondingEdge.getNetwork();
					if (memberNetwork != null){
						hiddenEdges.get(memberNetwork).add(correspondingEdge.getEdge());
					}
				}
			}
		}
		
		
		//prepare the CyEdit so that the deletion can be undone
		ArrayList<AbstractCyEdit> deletions = new ArrayList<AbstractCyEdit>();
		deletions.add(new DeleteEdit((CySubNetwork)unionNetwork, hiddenNodes.get(unionNetwork), new HashSet<CyEdge>(hiddenEdges.get(unionNetwork)), appAdapter.getCyNetworkViewManager(), appAdapter.getVisualMappingManager(), appAdapter.getCyEventHelper()));
		for (CyNetwork memberNetwork : dynet.getMemberNetworks()){
			deletions.add(new DeleteEdit((CySubNetwork)memberNetwork, hiddenNodes.get(memberNetwork), new HashSet<CyEdge>(hiddenEdges.get(memberNetwork)), appAdapter.getCyNetworkViewManager(), appAdapter.getVisualMappingManager(), appAdapter.getCyEventHelper()));
		}
		
		EditGroup editGroup = new EditGroup("Dynet: delete filtered", deletions){
			@Override
			public void redo() {
				super.redo();
				controlPanel.updateView();
			}

			@Override
			public void undo() {
				super.undo();
				controlPanel.updateView();
			}
		};
		
		appAdapter.getUndoSupport().postEdit(editGroup);
		editGroup.redo();   //do the actual deletion
	}

	
	/**
	 * Listens to when a column is deleted and update the filter
	 */
	@Override
	public void handleEvent(ColumnDeletedEvent e) {
		if (e.getSource() == dynet.getUnionNetwork().getDefaultNodeTable() ||
				e.getSource() == dynet.getUnionNetwork().getDefaultEdgeTable()){
			((DefaultTreeModel)filterTree.getModel()).reload();
		}
	}

	/**
	 * Listens to when a new column is created and update the filter
	 */
	@Override
	public void handleEvent(ColumnCreatedEvent e) {
		if (e.getSource() == dynet.getUnionNetwork().getDefaultNodeTable() ||
				e.getSource() == dynet.getUnionNetwork().getDefaultEdgeTable()){
			((DefaultTreeModel)filterTree.getModel()).reload();
		}
	}
}
