package eu.primes.dynet.internal.filter;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.cytoscape.model.CyColumn;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyTable;

import eu.primes.dynet.internal.DynamicNetwork;

/**
 * This is the group of columns that a single CyRowFilter will examine.
 * 
 * @author Ivan Hendy Goenawan
 */
public class ColumnGroup{
	private String name;
	private CyTable table;
	private Class<?> type;
	private boolean joinTypeAnd;   //if true, join type is 'and', otherwise 'or'
	private HashMap<CyColumn, CyNetwork> columnToNetworkMap;
	
	/**
	 * The constructor for when we want to make a filter that only evaluates one column. 
	 * The nodeColumn parameter determines whether the column is a node or an edge attribute. 
	 */
	public ColumnGroup(DynamicNetwork dynet, CyColumn column, boolean nodeColumn){
		columnToNetworkMap = new HashMap<CyColumn, CyNetwork>();
		
		if (nodeColumn){
			name = "Node: " + column.getName();
		}else{
			name = "Edge: " + column.getName();
		}
		
		table = column.getTable();
		type = column.getType();
		joinTypeAnd = true;
		
		columnToNetworkMap.put(column, dynet.getNetworkOfColumn(column));
	}
	
	
	/**
	 * The constructor for when we want to make a filter that only evaluates all columns from all member networks
	 * that have the same attributeName. The nodeColumn parameter determines whether the column is a node or
	 * an edge attribute. The joinTypeAnd parameter determines if the join type is 'and' or 'or. If it's 'and'
	 * then only nodes/edges that fulfill the criteria in all networks will be shown in the union network.
	 * Otherwise, nodes/edges that meet the criteria in at least one network will be shown.
	 *
	 */
	public ColumnGroup(DynamicNetwork dynet, String attributeName, boolean nodeColumn, boolean joinTypeAnd) throws Exception{
		columnToNetworkMap = new HashMap<CyColumn, CyNetwork>();
		CyNetwork unionNetwork = dynet.getUnionNetwork();
		table = nodeColumn ? unionNetwork.getDefaultNodeTable() : unionNetwork.getDefaultEdgeTable();
		
		for (CyNetwork network : dynet.getMemberNetworks()){
			String columnName = network.getRow(network).get(CyNetwork.NAME, String.class) + "_" + attributeName;
			CyColumn column = table.getColumn(columnName);
			if (column == null) throw new Exception ("ColumnGroup failed to initialize: attribute not present in all networks");
			
			if (type == null){
				type = column.getType();
			}else{
				if (type != column.getType()){
					throw new Exception ("ColumnGroup failed to initialize: not all columns have the same type");
				}
			}
			
			columnToNetworkMap.put(column, network);
		}
		
		this.joinTypeAnd = joinTypeAnd;
		name = "";
		if (nodeColumn){
			name += "Node: ";
		}else{
			name = "Edge: ";
		}
		if (joinTypeAnd){
			name += "ALL_" + attributeName;
		}else{
			name += "EACH_" + attributeName;
		}
	}
	
	@Override
	public String toString() {
		return name;
	}

	public String getName(){
		return name;
	}
	
	
	/**
	 * If network is not given (null) then all columns will be returned.
	 * Otherwise return only columns specific to the network + columns that are not specific to any network.
	 */
	public Set<CyColumn> getColumns(CyNetwork network){
		Set<CyColumn> result = new HashSet<CyColumn>();
		for (CyColumn column : columnToNetworkMap.keySet()){
			if (network == null || columnToNetworkMap.get(column) == null || columnToNetworkMap.get(column) == network){
				result.add(column);
			}
		}
		return result;
	}
	
	public Class<?> getType(){
		return type;
	}
	
	public CyTable getTable(){
		return table;
	}
	
	public boolean isJoinTypeAnd(){
		return joinTypeAnd;
	}

	@Override
	public boolean equals(Object other) {
		if (other.getClass() != ColumnGroup.class) return false;
		return this.columnToNetworkMap.equals(((ColumnGroup)other).columnToNetworkMap) && this.joinTypeAnd == ((ColumnGroup)other).joinTypeAnd;
	}
}