package eu.primes.dynet.internal.filter;

import java.awt.event.ActionListener;

import javax.swing.JTree;
import javax.swing.UIManager;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.plaf.basic.BasicTreeUI;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import eu.primes.dynet.internal.DynamicNetwork;

/**
 * This is the tree structure that stores the actual filters. The JTree has been modified so that they are always expanded.
 * 
 * @author Ivan Hendy Goenawan
 */
public class FilterTree extends JTree {

	private DynamicNetwork dynet;
	private DefaultTreeModel treeModel;
	private ActionListener listener;
	
	public FilterTree(DynamicNetwork dynet, ActionListener listener){
		this.dynet = dynet;
		this.listener = listener;
		
		treeModel = new DefaultTreeModel(new FilterNode());
		treeModel.addTreeModelListener(new TreeModelListener() {
			@Override
			public void treeStructureChanged(TreeModelEvent e) {
				for (int i = 0; i < getRowCount(); i++){
					expandRow(i);
				}
			}
			@Override
			public void treeNodesRemoved(TreeModelEvent e) {
			}
			@Override
			public void treeNodesInserted(TreeModelEvent e) {
			}
			@Override
			public void treeNodesChanged(TreeModelEvent e) {
			}
		});
		setModel(treeModel);
		
		
		setRowHeight(0);  //so that row height is calculated dynamically
		setEditable(true);
		getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
		((BasicTreeUI)getUI()).setCollapsedIcon(null);
		((BasicTreeUI)getUI()).setExpandedIcon(null);
		setBackground(UIManager.getColor("Panel.background"));
		
		
		setCellRenderer(new FilterTreeCellRenderer(this, dynet, listener));
		setCellEditor(new FilterTreeCellEditor(this, dynet, listener));
		
	}

	
	/**
	 * Prevent tree from being collapsed.
	 */
	@Override
	protected void setExpandedState(TreePath path, boolean state) {
		if (state) super.setExpandedState(path, state);
	}

	
	
	
}
