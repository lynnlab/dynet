package eu.primes.dynet.internal.filter;

import org.cytoscape.model.CyColumn;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyRow;

/**
 * A filter that can evaluate CyRows based on a String attribute.
 * 
 * @author Ivan Hendy Goenawan
 */
public class StringCyRowFilter extends CyRowFilter {
	
	private String value;
	
	public enum FilterType{
		CONTAINS("contains"), NOT_CONTAINS("doesn't contain"), EQUALS("is"), NOT_EQUALS("is not");
		
		private final String symbol;
		FilterType(String symbol){
			this.symbol = symbol;
		}
		@Override
		public String toString() {
			return this.symbol;
		}
	}
	
	private FilterType filterType;
	private boolean caseSensitive;
	
	
	public StringCyRowFilter(ColumnGroup columnGroup) {
		super(columnGroup);
	}
	
	
	/**
	 * Evaluates whether or not the row matches the specified filtering criteria.
	 * 
	 * If filter information (column or filter type) is not complete, return null for undefined result.
	 * If the column's table is not the same as the evaluated row's table (e.g. when an edge filter is used to evaluate a node), also return null.
	 * If either one of the filter's value or the actual row's value is null, return false (considered as not matching).
	 * 
	 * If network is not given (null), the filter should evaluate all columns in the columngroup.
	 * Otherwise only evaluate columns specific to the network + columns not specific to any network.
	 */
	@Override
	public Boolean evaluate(CyRow row, CyNetwork network) {
		if (columnGroup == null || filterType == null) return null;
		if (columnGroup.getTable() != row.getTable()) return null;
		if (value == null) return false;
		
		Boolean result = null;
		
		for (CyColumn column : columnGroup.getColumns(network)){
			String rowValue = row.get(column.getName(), String.class);
			
			boolean tempResult;
			if (rowValue == null){
				tempResult = false;
			}else{
				switch (filterType){
					case CONTAINS:
						if (caseSensitive){
							tempResult = rowValue.contains(value);
						}else{
							tempResult = rowValue.toLowerCase().contains(value.toLowerCase());
						}
						break;
					case NOT_CONTAINS:
						if (caseSensitive){
							tempResult = !rowValue.contains(value);
						}else{
							tempResult = !rowValue.toLowerCase().contains(value.toLowerCase());
						}
						break;
					case EQUALS:
						if (caseSensitive){
							tempResult = rowValue.equals(value);
						}else{
							tempResult = rowValue.equalsIgnoreCase(value);
						}
						break;
					case NOT_EQUALS:
						if (caseSensitive){
							tempResult = !rowValue.equals(value);
						}else{
							tempResult = !rowValue.equalsIgnoreCase(value);
						}
						break;
					default:
						tempResult = false;
				}
			}
			
			if (columnGroup.isJoinTypeAnd()){
				if (result == null) result = tempResult;
				else result &= tempResult;
				if (!result) break;
			}else{
				if (result == null) result = tempResult;
				else result |= tempResult;
				if (result) break;
			}
			
		}
		
		return result;
	}
	
	public boolean getCaseSensitive(){
		return caseSensitive;
	}
	
	public void setCaseSensitive(boolean caseSensitive){
		this.caseSensitive = caseSensitive;
	}
	
	public FilterType getFilterType(){
		return filterType;
	}
	
	public void setFilterType(FilterType filterType){
		this.filterType = filterType;
	}
	
	public String getValue(){
		return value;
	}
	
	public void setValue(String value){
		this.value = value;
	}
}
