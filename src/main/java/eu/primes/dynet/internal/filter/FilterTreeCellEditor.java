package eu.primes.dynet.internal.filter;

import java.awt.Component;
import java.awt.event.ActionListener;

import javax.swing.AbstractCellEditor;
import javax.swing.JTree;
import javax.swing.tree.TreeCellEditor;

import eu.primes.dynet.internal.DynamicNetwork;

/**
 * The editor for each node in the FilterTree. Returns either a RootFilterTreeCell or a FilterTreeCell depending on
 * whether or not the node is the root node. 
 * 
 * @author Ivan Hendy Goenawan
 */
public class FilterTreeCellEditor extends AbstractCellEditor implements TreeCellEditor {

	private JTree filterTree;
	private DynamicNetwork dynet;
	private ActionListener listener;
	
	public FilterTreeCellEditor(JTree filterTree, DynamicNetwork dynet, ActionListener listener){
		this.filterTree = filterTree;
		this.dynet = dynet;
		this.listener = listener;
	}

	@Override
	public Object getCellEditorValue() {
		return null;
	}

	@Override
	public Component getTreeCellEditorComponent(JTree tree, Object value,
			boolean isSelected, boolean expanded, boolean leaf, int row) {
		
		if (row == 0) return new RootFilterTreeCell((FilterNode)value, filterTree, listener);
		else return new FilterTreeCell((FilterNode) value, dynet, filterTree, listener);
	}
	
	

}
