package eu.primes.dynet.internal;

import java.awt.Dimension;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.cytoscape.app.swing.CySwingAppAdapter;
import org.cytoscape.application.swing.CyNetworkViewDesktopMgr;
import org.cytoscape.application.swing.CytoPanelComponent;
import org.cytoscape.application.swing.CytoPanelName;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.events.NetworkAboutToBeDestroyedEvent;
import org.cytoscape.model.events.NetworkAboutToBeDestroyedListener;
import org.cytoscape.service.util.AbstractCyActivator;
import org.cytoscape.service.util.CyServiceRegistrar;
import org.cytoscape.task.NodeViewTaskFactory;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.model.events.NetworkViewAboutToBeDestroyedEvent;
import org.cytoscape.view.model.events.NetworkViewAboutToBeDestroyedListener;
import org.cytoscape.work.TaskFactory;
import org.osgi.framework.BundleContext;

import eu.primes.dynet.internal.filter.FilterPanel;
import eu.primes.dynet.internal.heatmap.HeatMapControlPanel;
import eu.primes.dynet.internal.initdialog.ShowInitDialogTaskFactory;
import eu.primes.dynet.internal.networkimporter.NetworkImportDialogTaskFactory;
import eu.primes.dynet.internal.nodeanalyzer.NodeAnalyzerTaskFactory;
import eu.primes.dynet.internal.pairwisecompare.PairwiseComparePanel;
import eu.primes.dynet.internal.variance.MultiNetworkVariationPanel;

/**
 * This is the activator class of the whole App. It provides the initial menu item for initializing the dynamic network and 
 * handles the subsequent registration of services such as user interface panels, listeners, etc.
 * 
 * In addition, the class also the class also lays out network views nicely on the screen with the union network shown in a big window and 
 * the member networks divided into smaller windows in the remaining space. 
 * 
 * The class also detects when the dynet union network has been destroyed and performs the necessary cleanup.
 * 
 * @author Ivan Hendy Goenawan
 */
public class DynetActivator extends AbstractCyActivator implements NetworkAboutToBeDestroyedListener, NetworkViewAboutToBeDestroyedListener{
	private CySwingAppAdapter swingAdapter;
	private DynamicNetwork dynet;
	private ArrayList<Object> services;
	
	private static final int LARGE_WINDOW_CUTOFF = 12;  //number of networks needed before reference network is shown in large window 
	
	
	@Override
	public void start(BundleContext context) throws Exception {
		swingAdapter = getService(context, CySwingAppAdapter.class);
		
		
		NetworkImportDialogTaskFactory networkImportTaskFactory = new NetworkImportDialogTaskFactory(swingAdapter);
		Properties networkImportTaskProperty = new Properties();
		networkImportTaskProperty.setProperty("title", "DyNet Network Importer");
		networkImportTaskProperty.setProperty("preferredMenu", "Apps");
		registerService(context, networkImportTaskFactory, TaskFactory.class, networkImportTaskProperty);
		
		
		ShowInitDialogTaskFactory dynetSetupTaskFactory = new ShowInitDialogTaskFactory(swingAdapter, this);
		Properties dynetSetupTaskProperty = new Properties();
		dynetSetupTaskProperty.put("title", "DyNet Analyzer");
		dynetSetupTaskProperty.put("preferredMenu", "Apps");
		registerService(context, dynetSetupTaskFactory, TaskFactory.class, dynetSetupTaskProperty);
		
		
		registerAllServices(context, this, new Properties());
	}

	/**
	 * This is called when the dynamic network constructor is finished. It goes on to lay out the network windows, register
	 * the control panels, etc.
	 */
	public void dynetSetupFinish(DynamicNetwork dynet) {
		CyServiceRegistrar registrar = swingAdapter.getCyServiceRegistrar();
		services = new ArrayList<Object>();
		
		this.dynet = dynet;
		registrar.registerAllServices(dynet, new Properties());
		services.add(dynet);
		
		//lay out all the network windows. If there are more networks than the specified CUTOFF, only the union network 
		//will be shown in one maximized window
		CyNetworkViewDesktopMgr netViewDesktopManager = registrar.getService(CyNetworkViewDesktopMgr.class);
		Dimension screen = netViewDesktopManager.getDesktopViewAreaSize();
		List<CyNetworkView> memberNetworkViews = dynet.getMemberNetworkViews();
		if (memberNetworkViews.size() <= LARGE_WINDOW_CUTOFF){
			netViewDesktopManager.setBounds(dynet.getUnionNetworkView(), new Rectangle(screen.width / 2, screen.height));
			
			List<Rectangle> positions = divideSpace(screen.width / 2, 0, screen.width / 2, screen.height, memberNetworkViews.size());
			for (int i = 0; i < memberNetworkViews.size(); i++){
				netViewDesktopManager.setBounds(memberNetworkViews.get(i), positions.get(i));
			}
			
		}else{
			netViewDesktopManager.setBounds(dynet.getUnionNetworkView(), new Rectangle(20,20, screen.width - 40, screen.height - 40));
			
			List<Rectangle> positions = divideSpace(0, 0, screen.width, screen.height, memberNetworkViews.size());
			for (int i = 0; i < memberNetworkViews.size(); i++){
				netViewDesktopManager.setBounds(memberNetworkViews.get(i), positions.get(i));
			}
		}
		dynet.getUnionNetworkView().fitContent();
		
		
		boolean nodePairwiseComparison = memberNetworkViews.size() > 2 ? false : true;
		boolean edgePairwiseComparison = memberNetworkViews.size() > 2 ? false : true;
		boolean nodeVarianceAnalysis = memberNetworkViews.size() > 2 ? true : false;
		boolean edgeVarianceAnalysis = false;
		
		
		ControlPanel controlPanel = new ControlPanel(dynet, swingAdapter);
		FilterPanel filterPanel = new FilterPanel(controlPanel, dynet, swingAdapter);
		PairwiseComparePanel pairwiseComparePanel = new PairwiseComparePanel(controlPanel, dynet, swingAdapter, nodePairwiseComparison, edgePairwiseComparison);
		MultiNetworkVariationPanel differenceMapperPanel = new MultiNetworkVariationPanel(controlPanel, dynet, swingAdapter, nodeVarianceAnalysis, edgeVarianceAnalysis);
		SyncSettingsPanel syncSettingsPanel = new SyncSettingsPanel(dynet, controlPanel);
		HeatMapControlPanel heatMapPanel = new HeatMapControlPanel(controlPanel, dynet, swingAdapter);
		controlPanel.addToMainPanel(pairwiseComparePanel);
		controlPanel.addToMainPanel(differenceMapperPanel);
		controlPanel.addToMainPanel(filterPanel);
		controlPanel.addToMainPanel(syncSettingsPanel);
		controlPanel.addToMainPanel(heatMapPanel);
		
		registrar.registerAllServices(heatMapPanel, new Properties());
		services.add(heatMapPanel);
		registrar.registerAllServices(syncSettingsPanel, new Properties());
		services.add(syncSettingsPanel);
		registrar.registerAllServices(filterPanel, new Properties());
		services.add(filterPanel);
		registrar.registerService(controlPanel, CytoPanelComponent.class, new Properties());
		services.add(controlPanel);
		int index = swingAdapter.getCySwingApplication().getCytoPanel(CytoPanelName.WEST).indexOfComponent(controlPanel);
		swingAdapter.getCySwingApplication().getCytoPanel(CytoPanelName.WEST).setSelectedIndex(index);
		
		
		
		NodeAnalyzerTaskFactory nodeAnalyzerTaskFactory = new NodeAnalyzerTaskFactory(dynet, swingAdapter, controlPanel);
		Properties nodeAnalyzerTaskProperty = new Properties();
		nodeAnalyzerTaskProperty.setProperty("title", "DyNet Node Analyzer");
		registrar.registerService(nodeAnalyzerTaskFactory, NodeViewTaskFactory.class, nodeAnalyzerTaskProperty);
		services.add(nodeAnalyzerTaskFactory);
	}
	
	public boolean dynetExists(){
		return (dynet != null);
	}
	
	/**
	 * Returns the list of bounds for the optimal division of a screen space into n windows. The x and y specifies the location of the 
	 * top left corner of the space to be divided, while the spaceWidth and spaceHeight specifies the space dimension.
	 */
	public List<Rectangle> divideSpace(int x, int y, int spaceWidth, int spaceHeight, int n){
		int row = 1;
		int col = 1;
		
		while (row * col < n){
			float widthIfAddRow = (float)spaceWidth / col;
			float heightIfAddRow = (float)spaceHeight / (row + 1);
			float ratioIfAddRow = widthIfAddRow > heightIfAddRow ? widthIfAddRow / heightIfAddRow : heightIfAddRow / widthIfAddRow;
			
			float widthIfAddCol = (float)spaceWidth / (col + 1);
			float heightIfAddCol = (float)spaceHeight / row;
			float ratioIfAddCol = widthIfAddCol > heightIfAddCol ? widthIfAddCol / heightIfAddCol : heightIfAddCol / widthIfAddCol;
			
			//add row or column so that the difference between each window's width and height is minimal, or in other words prefer
			//square windows
			if (ratioIfAddRow < ratioIfAddCol){
				row++;
			}else{
				col++;
			}
		}
		
		
		ArrayList<Rectangle> positions = new ArrayList<Rectangle>();
		int width = (int)((float)spaceWidth / col);
		int height = (int)((float)spaceHeight / row);
		
		for (int i = 0; i < n; i++){
			int currentRow = i / col;
			int currentCol = i % col;
			
			positions.add(new Rectangle(x + (currentCol * width), y + (currentRow * height), width, height));
		}
		
		return positions;
	}

	/**
	 * Performs cleanup when the union network is destroyed.
	 */
	@Override
	public void handleEvent(NetworkAboutToBeDestroyedEvent e) {
		if (dynet != null && e.getNetwork() == dynet.getUnionNetwork()){
			dynet.cleanUp();
			dynet = null;
			
			for (Object service : services){
				swingAdapter.getCyServiceRegistrar().unregisterAllServices(service);
			}
			services = null;
		}
	}
	
	/**
	 * Performs cleanup when the union network view is closed.
	 */
	@Override
	public void handleEvent(NetworkViewAboutToBeDestroyedEvent e) {
		if (dynet != null && e.getNetworkView() == dynet.getUnionNetworkView()){
			dynet.cleanUp();
			CyNetwork unionNetwork = dynet.getUnionNetwork();
			dynet = null;
			
			for (Object service : services){
				swingAdapter.getCyServiceRegistrar().unregisterAllServices(service);
			}
			services = null;
			
			swingAdapter.getCyNetworkManager().destroyNetwork(unionNetwork);
		}
	}

	/**
	 * Performs cleanup when the whole app is shut down.
	 */
	@Override
	public void shutDown() {		
		if (dynet != null){
			swingAdapter.getCyNetworkManager().destroyNetwork(dynet.getUnionNetwork());
			dynet = null;
		}
		super.shutDown();
	}
}
