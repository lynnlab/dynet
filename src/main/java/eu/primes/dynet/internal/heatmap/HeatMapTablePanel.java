package eu.primes.dynet.internal.heatmap;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableColumnModelEvent;
import javax.swing.event.TableColumnModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import org.cytoscape.model.CyEdge;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyRow;

import eu.primes.dynet.internal.DynamicNetwork;

/**
 * This panel shows the actual heatmap, which is based on a JTable. It accepts the roots of the two dendograms
 * (for both y-axis (edges) and x-axis (networks) contained in TreeNode objects. The heatmap can also listen
 * to user's selecting rows (edges) on the heatmap and responds by selecting corresponding edges in the network views.
 * 
 * @author Ivan Hendy Goenawan
 */

public class HeatMapTablePanel extends JPanel{
	private DynamicNetwork dynet;
	private List<CyNetwork> selectedNetworks;
	private TreeNode edgeRootNode;
	private double maxValue;
	private JTable heatMapTable;
	private TreeNode networkRootNode;
	private HashMap<CyEdge, List<Double>> edgeToValuesMap;
	private HashMap<CyEdge, Integer> edgeToHeatMapRowMap;  //used for selecting rows in heatmap when edges are selected
	private ListSelectionListener heatMapListener;
	
	private int rowHeight = 4;
	private int maxEdgeTreeLevel = 0;
	private int maxNetworkTreeLevel = 0;
	private Color lowColor = Color.GREEN;       //for values lower than 0
	private Color midColor = Color.WHITE;       //for 0
	private Color highColor = Color.RED;        //for values greater than 0
	
	
	private TableColumn resizedColumn;
	
	
	public HeatMapTablePanel(DynamicNetwork dynet, List<CyNetwork> selectedNetworks, HashMap<CyEdge, List<Double>> edgeToValuesMap, TreeNode edgeRootNode, TreeNode networkRootNode){
		this.dynet = dynet;
		this.selectedNetworks = selectedNetworks;
		this.edgeToValuesMap = edgeToValuesMap;
		this.edgeRootNode = edgeRootNode;
		this.networkRootNode = networkRootNode;
		
		
		final ArrayList<TreeNode> clusteredEdgeNodeList = new ArrayList<TreeNode>();
		maxEdgeTreeLevel = convertTreeToNodeList(edgeRootNode, clusteredEdgeNodeList);
		
		edgeToHeatMapRowMap = new HashMap<CyEdge, Integer>();
		
		
		for(int i = 0; i < clusteredEdgeNodeList.size(); i++){
			TreeNode edgeNode = clusteredEdgeNodeList.get(i);
			CyEdge edge = (CyEdge)edgeNode.getObject();
			edgeToHeatMapRowMap.put(edge, i);
			
			for(Double value : edgeToValuesMap.get(edge)){
				if (Math.abs(value) > maxValue){
					maxValue = Math.abs(value);
				}
			}
		}
		
		
		heatMapTable = new JTable();
		heatMapTable.setShowGrid(false);
		heatMapTable.setRowHeight(rowHeight);
		heatMapTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		heatMapTable.setModel(new AbstractTableModel() {
			
			@Override
			public Object getValueAt(int rowIndex, int columnIndex) {
				CyEdge edge = (CyEdge) clusteredEdgeNodeList.get(rowIndex).getObject();
				return HeatMapTablePanel.this.edgeToValuesMap.get(edge).get(columnIndex);
			}
			
			@Override
			public int getRowCount() {
				return clusteredEdgeNodeList.size();
			}
			
			@Override
			public int getColumnCount() {
				return HeatMapTablePanel.this.selectedNetworks.size();
			}

			@Override
			public Class<?> getColumnClass(int columnIndex) {
				return Double.class;
			}

			@Override
			public String getColumnName(int column) {
				CyNetwork network = HeatMapTablePanel.this.selectedNetworks.get(column);
				String networkName = network.getRow(network).get(CyNetwork.NAME, String.class);
				return networkName;
			}
			
		});
		heatMapTable.setIntercellSpacing(new Dimension(0,0));
		heatMapTable.setDefaultRenderer(Double.class, new TableCellRenderer() {
			@Override
			public Component getTableCellRendererComponent(JTable table, Object value,
					boolean isSelected, boolean hasFocus, int row, int column) {
				JPanel panel = new JPanel();
				Color color = getColor((Double)value, maxValue);
				if (isSelected){
					Color selectionColor = UIManager.getColor("List.selectionBackground");
					int r = (color.getRed() + selectionColor.getRed()) / 2;
					int g = (color.getGreen() + selectionColor.getGreen()) / 2;
					int b = (color.getBlue() + selectionColor.getBlue()) / 2;
					panel.setBackground(new Color(r,g,b));
				}else{
					panel.setBackground(color);
				}
				return panel;
			}
		});
		
		heatMapTable.getColumnModel().addColumnModelListener(new TableColumnModelListener() {
			
			private boolean resizing = false;
			
			@Override
			public void columnSelectionChanged(ListSelectionEvent e) {
			}
			
			@Override
			public void columnRemoved(TableColumnModelEvent e) {
			}
			
			@Override
			public void columnMoved(TableColumnModelEvent e) {
			}
			
			@Override
			public void columnMarginChanged(ChangeEvent e) {
				if (heatMapTable.getTableHeader().getResizingColumn() != null){
					resizedColumn = heatMapTable.getTableHeader().getResizingColumn();
				}
			}
			
			@Override
			public void columnAdded(TableColumnModelEvent e) {
			}
		});
		
		heatMapTable.getTableHeader().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				if (resizedColumn != null){
					int width = resizedColumn.getWidth();
					for (int i = 0; i < heatMapTable.getColumnCount(); i++){
						TableColumn column = heatMapTable.getColumnModel().getColumn(i);
						if (column != resizedColumn){
							column.setPreferredWidth(width);
						}
					}
					resizedColumn = null;
				}
			}
		});
		
		
		heatMapListener = new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if (!e.getValueIsAdjusting()){
					CyNetwork unionNetwork = HeatMapTablePanel.this.dynet.getUnionNetwork();
					for (int i = e.getFirstIndex(); i <= e.getLastIndex(); i++){
						CyRow row = unionNetwork.getRow((CyEdge)clusteredEdgeNodeList.get(i).getObject());
						row.set(CyNetwork.SELECTED, heatMapTable.getSelectionModel().isSelectedIndex(i));
					}
				}
			}
		};
		
		heatMapTable.getSelectionModel().addListSelectionListener(heatMapListener);
		
		
		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				clearSelection();
			}
		});
		
		
		
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{10, 0, 0, 10, 0};
		gridBagLayout.rowHeights = new int[]{10, 0, 0, 0, 10, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		GridBagConstraints gbc_tableHeader = new GridBagConstraints();
		gbc_tableHeader.fill = GridBagConstraints.BOTH;
		gbc_tableHeader.gridx = 1;
		gbc_tableHeader.gridy = 2;
		add(heatMapTable.getTableHeader(), gbc_tableHeader);
		
		GridBagConstraints gbc_heatMapTable = new GridBagConstraints();
		gbc_heatMapTable.fill = GridBagConstraints.BOTH;
		gbc_heatMapTable.gridx = 1;
		gbc_heatMapTable.gridy = 3;
		add(heatMapTable, gbc_heatMapTable);
		
		GridBagConstraints gbc_sideDendogram = new GridBagConstraints();
		gbc_sideDendogram.fill = GridBagConstraints.BOTH;
		gbc_sideDendogram.gridx = 2;
		gbc_sideDendogram.gridy = 3;
		add(new RightDendogram(edgeRootNode, clusteredEdgeNodeList.size(), maxEdgeTreeLevel), gbc_sideDendogram);
		
		//reorder columns if networks are also clustered
		if (networkRootNode != null){
			ArrayList<TreeNode> clusteredNetworkNodeList = new ArrayList<TreeNode>();
			maxNetworkTreeLevel = convertTreeToNodeList(networkRootNode, clusteredNetworkNodeList);
			
			for (TreeNode networkNode : clusteredNetworkNodeList){
				CyNetwork network = (CyNetwork)networkNode.getObject();
				String networkName = network.getRow(network).get(CyNetwork.NAME, String.class);
				TableColumn column = heatMapTable.getColumn(networkName);
				heatMapTable.getColumnModel().removeColumn(column);
				heatMapTable.getColumnModel().addColumn(column);
			}
			
			heatMapTable.getTableHeader().setReorderingAllowed(false);
			
			GridBagConstraints gbc_topDendogram = new GridBagConstraints();
			gbc_topDendogram.fill = GridBagConstraints.BOTH;
			gbc_topDendogram.gridx = 1;
			gbc_topDendogram.gridy = 1;
			add(new TopDendogram(networkRootNode,clusteredNetworkNodeList.size(), maxNetworkTreeLevel), gbc_topDendogram);
		}
		
		
	}
	
	public void clearSelection(){
		heatMapTable.getSelectionModel().clearSelection();
	}
	
	public void setEdgeSelection(CyEdge edge, boolean selection){
		Integer index = edgeToHeatMapRowMap.get(edge);
		if (index != null){
			heatMapTable.getSelectionModel().removeListSelectionListener(heatMapListener);
			
			if (selection){
				heatMapTable.getSelectionModel().addSelectionInterval(index, index);
			}else{
				heatMapTable.getSelectionModel().removeSelectionInterval(index, index);
			}
			
			heatMapTable.getSelectionModel().addListSelectionListener(heatMapListener);
		}
	}
	
	//convert dendogram tree to a list (containing only leaf nodes) in a depth-first-search manner
	private int convertTreeToNodeList(TreeNode rootNode, List<TreeNode> destinationList){
		if (rootNode.isLeaf()){
			destinationList.add(rootNode);
			return 0;
		}else{
			int maxLevel = 0;
			for (TreeNode child : rootNode.getChildren()){
				int level = convertTreeToNodeList(child, destinationList);
				if (level > maxLevel) maxLevel = level;
			}
			return maxLevel + 1;
		}
	}
	
	
	
	private Color getColor(double value, double maxValue){
		double fractionDistance = 0;
		if (maxValue != 0) fractionDistance = Math.abs(value) / maxValue;
		
		int r, g, b;
		if (value >=0){
			r = (int) Math.round(fractionBetweenTwoNumbers(midColor.getRed(), highColor.getRed(), fractionDistance));
			g = (int) Math.round(fractionBetweenTwoNumbers(midColor.getGreen(), highColor.getGreen(), fractionDistance));
			b = (int) Math.round(fractionBetweenTwoNumbers(midColor.getBlue(), highColor.getBlue(), fractionDistance));
		}else{
			r = (int) Math.round(fractionBetweenTwoNumbers(midColor.getRed(), lowColor.getRed(), fractionDistance));
			g = (int) Math.round(fractionBetweenTwoNumbers(midColor.getGreen(), lowColor.getGreen(), fractionDistance));
			b = (int) Math.round(fractionBetweenTwoNumbers(midColor.getBlue(), lowColor.getBlue(), fractionDistance));
		}
		return new Color(r,g,b);
	}
	
	//calculate points between two numbers that are a 'fraction' of the total distance
	//e.g. The point that is 0.6 of the distance between 11 and 15 = 11 + (0.6 * 4) = 13.4
	private double fractionBetweenTwoNumbers(double start, double stop, double fraction){
		double interval = Math.abs(stop - start);
		double step = fraction * interval;
		
		if (stop > start) return start + step;
		else if (stop < start) return start - step;
		else return start;
	}
}
