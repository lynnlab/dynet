package eu.primes.dynet.internal.heatmap;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;

import org.cytoscape.app.CyAppAdapter;
import org.cytoscape.model.CyColumn;
import org.cytoscape.model.CyEdge;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.model.View;
import org.cytoscape.view.presentation.property.BasicVisualLexicon;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;

import eu.primes.dynet.internal.DynamicNetwork;

/**
 * The task that generates the heatmap. It performs the hierarchical clustering, construct the HeatMapTablePanel object and add it
 * to the HeatMapPanel.
 * 
 * @author Ivan Hendy Goenawan
 */
public class GenerateHeatMapTask extends AbstractTask{
	private HeatMapControlPanel parent;
	private CyAppAdapter appAdapter;
	private DynamicNetwork dynet;
	private List<CyNetwork> selectedNetworks;
	private String edgeProperty;
	private boolean clusterEdgeValues;
	private boolean clusterNetworks;
	private CyNetwork unionNetwork;
	private CyNetworkView unionNetworkView;
	
	//only works correctly for max number of 1 million items, can be extended in the future if needed.
	private double[] cosSimLookupTable = new double[1000000];  
	private boolean[] cosSimLookupStatus = new boolean[1000000];
	
	
	public GenerateHeatMapTask(HeatMapControlPanel parent, CyAppAdapter appAdapter, DynamicNetwork dynet, List<CyNetwork> selectedNetworks, String edgeProperty, boolean clusterEdgeValues, boolean clusterNetworks){
		this.parent = parent;
		this.appAdapter = appAdapter;
		this.dynet = dynet;
		this.selectedNetworks = selectedNetworks;
		this.edgeProperty = edgeProperty;
		this.clusterEdgeValues = clusterEdgeValues;
		this.clusterNetworks = clusterNetworks;
		
		this.unionNetwork = dynet.getUnionNetwork();
		this.unionNetworkView = dynet.getUnionNetworkView();
	}
	
	
	@Override
	public void run(TaskMonitor taskMonitor) throws Exception {
		
		ArrayList<CyEdge> edgeList = new ArrayList<CyEdge>();
		for (View<CyEdge> edgeView : unionNetworkView.getEdgeViews()){
			if (edgeView.getVisualProperty(BasicVisualLexicon.EDGE_VISIBLE)){
				edgeList.add(edgeView.getModel());
			}
		}
		
		
		HashMap<CyEdge, List<Double>> edgeToValuesMap = new HashMap<CyEdge, List<Double>>();
		for (CyEdge edge : edgeList){
			edgeToValuesMap.put(edge, getEdgeValues(edge));
		}
		
		//for each run of the task, reset the auto-generated ID of TreeNode so that it doesn't get higher and higher, potentially
		//exceeding the current 1 million limit
		TreeNode.resetCounter();  
		
		
		taskMonitor.setTitle("Heatmap generator: clustering edges");
		
		ArrayList<TreeNode> edgeTreeNodes = new ArrayList<TreeNode>();
		TreeNode edgeRootNode;
		
		if (clusterEdgeValues){
			for (CyEdge edge : edgeList){
				edgeTreeNodes.add(new TreeNode(edge, edgeToValuesMap.get(edge)));
			}
			
			edgeRootNode = clusterTreeNodes(edgeTreeNodes, new DistanceCalculator(){
				@Override
				public double calculateDistance(TreeNode nodeA, TreeNode nodeB) {
					return 1 - cosineSimilarity(nodeA, nodeB);
				}
			}, taskMonitor);
		}else{  //cluster based on edge proximity in the network
			for (CyEdge edge : edgeList){
				edgeTreeNodes.add(new TreeNode(edge, getEdgeCoordinates(edge)));
			}
			
			edgeRootNode = clusterTreeNodes(edgeTreeNodes, new DistanceCalculator(){
				@Override
				public double calculateDistance(TreeNode nodeA, TreeNode nodeB) {
					return squaredEuclideanDistance(nodeA.getValues(), nodeB.getValues());
				}
			}, taskMonitor);
		}
		
		if (cancelled) return;
		
		TreeNode networkRootNode = null;
		if (clusterNetworks){
			ArrayList<TreeNode> networkTreeNodes = new ArrayList<TreeNode>();
			for (CyNetwork network : selectedNetworks){
				networkTreeNodes.add(new TreeNode(network, getNetworkValues(network, edgeList)));
			}
			networkRootNode = clusterTreeNodes(networkTreeNodes, new DistanceCalculator(){
				@Override
				public double calculateDistance(TreeNode nodeA, TreeNode nodeB) {
					return 1 - cosineSimilarity(nodeA, nodeB);
				}
			}, null);
		}
		
		if (cancelled) return;
		
		appAdapter.get_DeselectAllTaskFactory().createTaskIterator(unionNetwork).next().run(taskMonitor);
		parent.setHeatMapFrame(new HeatMapFrame(dynet, selectedNetworks, edgeToValuesMap, edgeRootNode, networkRootNode));
	}
	
	/**
	 * For each edge, return a list of their "edgeProperty" values in the different networks specified in "selectedNetworks".
	 * The order of the values reflects the order of the network in "selectedNetworks", to maintain consistency across
	 * different edges.
	 */
	public List<Double> getEdgeValues(CyEdge edge){
		ArrayList<Double> values = new ArrayList<Double>();
		for (CyNetwork network : selectedNetworks){
			String attributeName = network.getRow(network).get(CyNetwork.NAME, String.class) + "_" + edgeProperty;
			Object value = unionNetwork.getRow(edge).get(attributeName, Object.class);
			
			if (value != null){
				Class<?> type = unionNetwork.getDefaultEdgeTable().getColumn(attributeName).getType();
				
				if (type == Boolean.class){
					if ((Boolean)value) value = new Double(1);
					else value = new Double(0);
				}else if (type == String.class){
					try{
						value = Double.parseDouble((String)value);
					}catch (Exception e){
						value = new Double(0);
					}
				}else{
					value = new Double(value.toString());
				}
			}else{
				value = new Double(0);
			}
			
			values.add((Double)value);
		}
		return values;
	}
	
	/**
	 * Get the mid-point of an edge.
	 */
	public List<Double> getEdgeCoordinates(CyEdge edge){
		ArrayList<Double> coordinates = new ArrayList<Double>();
		View<CyNode> sourceNodeView = unionNetworkView.getNodeView(edge.getSource());
		View<CyNode> targetNodeView = unionNetworkView.getNodeView(edge.getTarget());
		
		double x = (sourceNodeView.getVisualProperty(BasicVisualLexicon.NODE_X_LOCATION) + 
				targetNodeView.getVisualProperty(BasicVisualLexicon.NODE_Y_LOCATION)) / 2;
		
		double y = (sourceNodeView.getVisualProperty(BasicVisualLexicon.NODE_Y_LOCATION) + 
				targetNodeView.getVisualProperty(BasicVisualLexicon.NODE_Y_LOCATION)) / 2;
		
		coordinates.add(x);
		coordinates.add(y);
		
		return coordinates;
	}
	
	/**
	 * For each network, return a list of "edgeProperty" values of the edges specified in the "edgeList" parameter.
	 * If a particular edge is absent from a network, or if its "edgeProperty" attribute value cannot be converted to a number,
	 * the value 0 will be used instead. The order of values will reflect the order of edges in the edgeList. Therefore, to maintain
	 * consistency across different networks, the method should be called with the same edgeList.
	 */
	public List<Double> getNetworkValues(CyNetwork network, List<CyEdge> edgeList){
		String attributeName = network.getRow(network).get(CyNetwork.NAME, String.class) + "_" + edgeProperty;
		Class<?> type = null;
		CyColumn column = unionNetwork.getDefaultEdgeTable().getColumn(attributeName);
		if (column != null){
			type = column.getType();
		}
		
		ArrayList<Double> values = new ArrayList<Double>();
		for (CyEdge edge : edgeList){
			Object value = unionNetwork.getRow(edge).get(attributeName, Object.class);
			
			if (value != null){
				if (type == Boolean.class){
					if ((Boolean)value) value = new Double(1);
					else value = new Double(0);
				}else if (type == String.class){
					try{
						value = Double.parseDouble((String)value);
					}catch (Exception e){
						value = new Double(0);
					}
				}else{
					value = new Double(value.toString());
				}
			}else{
				value = new Double(0);
			}
			
			values.add((Double)value);
		}
		return values;
	}
	
	/**
	 * Performs hierarchical clustering of TreeNodes. First the most similar pair is joined and their values are averaged. Then we
	 * recalculate the distance of this new node to all the other existing nodes, and we repeat the process until all nodes are joined.
	 */
	public TreeNode clusterTreeNodes(List<TreeNode> leafNodes, DistanceCalculator distanceCalculator, TaskMonitor taskMonitor){
		LinkedList<TreeNode> unclusteredNodes = new LinkedList<TreeNode>(leafNodes);
		
		//this data structure allows for quick retrieval of the pair with shortest distance
		int initialCapacity = (unclusteredNodes.size() * unclusteredNodes.size() - unclusteredNodes.size())/ 2;
		PriorityQueue<NodePair> rankedPairs = new PriorityQueue<NodePair>(initialCapacity, new Comparator<NodePair>() {
			@Override
			public int compare(NodePair pairA, NodePair pairB) {
				if (pairA.getDistance() < pairB.getDistance()){
					return -1;
				}else if (pairA.getDistance() > pairB.getDistance()){
					return +1;
				}else{
					return 0;
				}
			}
		});
		
		boolean[] clusteredNodes = new boolean[1000000]; //lookup table for checking which nodes have already been clustered.
		
		
		int toDo = 0;
		int completed = 0;
		if (taskMonitor != null){
			taskMonitor.setStatusMessage("Calculating pairwise similarity");
			toDo = (unclusteredNodes.size() * unclusteredNodes.size()) / 2;  //total number of pairs
		}
		
		//first, calculate all pairwise similarity
		for (int i = 0; i < unclusteredNodes.size(); i++){
			TreeNode nodeA = unclusteredNodes.get(i);
			for (int j = i + 1; j < unclusteredNodes.size(); j++){
				if (cancelled) return null;
				
				TreeNode nodeB = unclusteredNodes.get(j);
				double distance = distanceCalculator.calculateDistance(nodeA, nodeB);
				rankedPairs.add(new NodePair(nodeA, nodeB, distance));
				
				if (taskMonitor != null){
					completed++;
					taskMonitor.setProgress((double)completed / toDo);
				}
			}
		}
		
		if (taskMonitor != null){
			taskMonitor.setStatusMessage("Performing hierarchical clustering");
			toDo = unclusteredNodes.size();
			completed = 0;
		}
		
		//then perform hierarchical clustering
		while(unclusteredNodes.size() > 1){
			NodePair mostSimilar = rankedPairs.remove();
			TreeNode nodeA = mostSimilar.getNodeA(); 
			TreeNode nodeB = mostSimilar.getNodeB();
			
			if (clusteredNodes[nodeA.getID()] || clusteredNodes[nodeB.getID()]){  //ignore pairs that contain already-clustered nodes
				continue;
			}else{
				clusteredNodes[nodeA.getID()] = true;
				clusteredNodes[nodeB.getID()] = true;
			}
			
			List<Double> valuesA = nodeA.getValues();
			List<Double> valuesB = nodeB.getValues();
			
			List<Double> combinedValues = new ArrayList<Double>();
			for (int i = 0; i < valuesA.size(); i++){
				combinedValues.add((valuesA.get(i) + valuesB.get(i)) / 2);
			}
			
			TreeNode newNode = new TreeNode(null, combinedValues);
			newNode.addChild(nodeA);
			newNode.addChild(nodeB);
			unclusteredNodes.remove(nodeA);
			unclusteredNodes.remove(nodeB);
			
			for (TreeNode oldNode : unclusteredNodes){
				if (cancelled) return null;
				
				double distance = distanceCalculator.calculateDistance(oldNode, newNode);
				rankedPairs.add(new NodePair(oldNode, newNode, distance));
			}
			
			unclusteredNodes.add(newNode);
			
			if (taskMonitor != null){
				completed++;
				taskMonitor.setProgress((double)completed / toDo);
			}
		}
		
		return unclusteredNodes.get(0);
	}
	
	
	/**
	 * Calculates the cosine similarity between the two vectors contained in the two TreeNode. The method is sped up with the use
	 * of a lookup table, taking advantage of the auto-generated ID of TreeNode objects.
	 */
	private double cosineSimilarity(TreeNode firstNode, TreeNode secondNode){
		double denominatorA, denominatorB;
		
		if (cosSimLookupStatus[firstNode.getID()]){
			denominatorA = cosSimLookupTable[firstNode.getID()];
		}else{
			denominatorA = computeNormValue(firstNode.getValues());
			cosSimLookupTable[firstNode.getID()] = denominatorA;
			cosSimLookupStatus[firstNode.getID()] = true;
		}
		
		if (cosSimLookupStatus[secondNode.getID()]){
			denominatorB = cosSimLookupTable[secondNode.getID()];
		}else{
			denominatorB = computeNormValue(secondNode.getValues());
			cosSimLookupTable[secondNode.getID()] = denominatorB;
			cosSimLookupStatus[secondNode.getID()] = true;
		}
		
		if (denominatorA == 0 || denominatorB == 0) return 1;
		
		List<Double> firstVector = firstNode.getValues();
		List<Double> secondVector = secondNode.getValues();
		
		double numerator = 0.0;
		for (int i = 0; i < firstVector.size(); i++){
			numerator += firstVector.get(i) * secondVector.get(i);
		}
		
		if (denominatorA == denominatorB && Math.sqrt(numerator) == denominatorA) return 1;
		return numerator / (denominatorA * denominatorB);
	}
	
	/**
	 * Compute the normal value of vectors, for obtaining the partial results for cosine similarity.
	 */
	private double computeNormValue(List<Double> values){
		double result = 0.0;
		for (double value : values){
			result += value * value;
		}
		return Math.sqrt(result);
	}
	
	/**
	 * Calculate the euclidean distance between two vectors but without taking the square root at the end.
	 * This still works because comparing the square of the distance should still give the same result when finding the
	 * pair of vector with the shortest distance. Eliminating the square root operation can hopefully speed up the operation significantly. 
	 */
	private static double squaredEuclideanDistance(List<Double> firstVector, List<Double> secondVector){
		double result = 0;
		double distance = 0;
		
		for (int i = 0; i < firstVector.size(); i++){			
			distance = secondVector.get(i) - firstVector.get(i);
			result += distance * distance;
		}
		
		return result;
	}
	
	
	//Storing a pair of items (either edge or networks) and their distance
	private class NodePair{
		private TreeNode nodeA;
		private TreeNode nodeB;
		private Double distance;
		
		public NodePair(TreeNode nodeA, TreeNode nodeB, Double distance){
			this.nodeA = nodeA;
			this.nodeB = nodeB;
			this.distance = distance;
		}
		
		public Double getDistance(){
			return distance;
		}
		
		public TreeNode getNodeA(){
			return nodeA;
		}
		
		public TreeNode getNodeB(){
			return nodeB;
		}
	}
	
	private abstract class DistanceCalculator{
		public abstract double calculateDistance(TreeNode nodeA, TreeNode nodeB);
	}
}
