package eu.primes.dynet.internal.heatmap;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.util.ArrayList;

import javax.swing.JPanel;

/**
 * This class draws the y-axis dendogram on the right side of the heatmap. For now the length of each line does not
 * indicate the distance between the objects. This can be changed in the future.
 * 
 * @author Ivan Hendy Goenawan
 */

public class RightDendogram extends JPanel{

	private TreeNode rootNode;
	private int nodeCount;
	private int maxLevel;
	private int branchLength = 2;
	
	private int width;
	private int height;
	private int rowHeight;
	private int currentRow;
	private Graphics g;
	
	public RightDendogram(TreeNode rootNode, int nodeCount, int maxLevel){
		this.rootNode = rootNode;
		this.nodeCount = nodeCount;
		this.maxLevel = maxLevel;
		
		this.setPreferredSize(new Dimension((maxLevel + 1) * branchLength, 0));
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		this.g = g;
		this.width = getWidth();
		this.height = getHeight();
		this.rowHeight = height / nodeCount;
		
		currentRow = 0;
		printNode(rootNode);
	}
	
	//Draws a dendogram TreeNode in a recursive manner. Tree is drawn bottom up from the leaf first.
	private Point printNode(TreeNode node){
		if (node.isLeaf()){
			int y = (currentRow * rowHeight) + (int)Math.round((double)rowHeight / 2);
			g.drawLine(0, y, branchLength, y);      //draw the horizontal branch line
			currentRow++;
			return new Point(branchLength, y);
		}else{
			int maxX = 0;
			int minY = height;
			int maxY = 0;
			
			ArrayList<Point> childNodePositions = new ArrayList<Point>();
			for (TreeNode child : node.getChildren()){
				Point endPosition = printNode(child);
				childNodePositions.add(endPosition);
				
				if (endPosition.x > maxX) maxX = endPosition.x;
				if (endPosition.y < minY) minY = endPosition.y;
				if (endPosition.y > maxY) maxY = endPosition.y;
			}
			
			g.drawLine(maxX, minY, maxX, maxY);     //connect the children with a vertical line
			
			//extends children that are not as long as its siblings
			for (Point endPosition : childNodePositions){
				g.drawLine(endPosition.x, endPosition.y, maxX, endPosition.y);
			}
			
			int midY = minY + (int)Math.round((double)(maxY-minY) / 2);
			g.drawLine(maxX, midY, maxX + branchLength, midY);        //draw the horizontal branch line
			return new Point(maxX + branchLength, midY);
		}
	}
}
