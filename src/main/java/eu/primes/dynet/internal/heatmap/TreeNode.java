package eu.primes.dynet.internal.heatmap;

import java.util.LinkedList;
import java.util.List;

/**
 * This class provides a tree structure that can be used for hierarchical clustering. Each newly created node is assigned
 * a unique ID to make it easy to do some kind of hashing.
 * 
 * @author Ivan Hendy Goenawan
 */

public class TreeNode {
	
	private Object object;
	private List<Double> values;
	
	private TreeNode parent;
	private List<TreeNode> children;
	private int ID;
	private static int counter = 0; 
	
	
	public TreeNode(Object object, List<Double> values){
		this.object = object;
		this.values = values;
		this.children = new LinkedList<TreeNode>();
		this.ID = counter;
		counter++;
	}
	
	public void addChild(TreeNode child){
		this.children.add(child);
		child.setParent(this);
	}
	
	public void removeChild(TreeNode child){
		child.setParent(null);
		this.children.remove(child);
	}
	
	public List<TreeNode> getChildren(){
		return children;
	}
	
	public void setParent(TreeNode parent){
		this.parent = parent;
	}
	
	public TreeNode getParent(){
		return parent;
	}
	
	public Object getObject(){
		return object;
	}
	
	public List<Double> getValues(){
		return values;
	}
	
	public boolean isLeaf(){
		return children.size() == 0;
	}
	
	public int getID(){
		return ID;
	}
	
	public static void resetCounter(){
		counter = 0;
	}
}
