package eu.primes.dynet.internal.heatmap;

import java.awt.FlowLayout;
import java.util.HashMap;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import org.cytoscape.model.CyEdge;
import org.cytoscape.model.CyNetwork;

import eu.primes.dynet.internal.DynamicNetwork;
import java.awt.BorderLayout;

public class HeatMapFrame extends JFrame{
	private JScrollPane scrollPane;
	private HeatMapTablePanel heatmap;
	
	
	public HeatMapFrame(DynamicNetwork dynet, List<CyNetwork> selectedNetworks, HashMap<CyEdge, List<Double>> edgeToValuesMap, TreeNode edgeRootNode, TreeNode networkRootNode){
		setTitle("DyNet Heatmap");
		setAlwaysOnTop(true);
		getContentPane().setLayout(new BorderLayout(0, 0));
		
		heatmap = new HeatMapTablePanel(dynet, selectedNetworks, edgeToValuesMap, edgeRootNode, networkRootNode);
		
		scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		scrollPane.setViewportView(heatmap);
		getContentPane().add(scrollPane, BorderLayout.CENTER);
	}
	
	
	public void setEdgeSelection(CyEdge edge, boolean selection){
		heatmap.setEdgeSelection(edge, selection);
	}
}
