package eu.primes.dynet.internal.variance;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.text.NumberFormat;
import java.util.List;
import java.util.Map;

import javax.swing.DefaultListSelectionModel;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;

import org.cytoscape.model.CyColumn;
import org.cytoscape.model.CyEdge.Type;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;

import eu.primes.dynet.internal.DynamicNetwork;

/**
 * This panel provides information about the node colour mapping scheme (focusing on a particular node) 
 * that has been applied by the NodeVarianceMapperTask (Multi-network Variance Analysis) if the user is comparing 
 * a regular node attribute and not nodes' connectivity.  It displays two tables, one containing the currently 
 * analyzed nodes, the other containing its direct neighbours. Both tables show the nodes's original attribute values 
 * in all networks included in the analysis, and the calculated variance.
 * 
 * @author Ivan Hendy Goenawan
 */

public class NodePropertyInformationPanel extends JPanel{
	
	private DynamicNetwork dynet;
	private List<CyNetwork> networks;
	private String nodeProperty;
	private Map<CyNode, Double> nodeVariationMap;
	private CyNode analyzedNode;
	private JTable neighbourTable;
	private JTable currentNodeTable;

	public NodePropertyInformationPanel(DynamicNetwork dynet, final List<CyNetwork> networks, final String nodeProperty, final Map<CyNode, Double> nodeVariationMap, final CyNode analyzedNode){
		this.dynet = dynet;
		this.networks = networks;
		this.nodeProperty = nodeProperty;
		this.analyzedNode = analyzedNode;
		this.nodeVariationMap =  nodeVariationMap;
		
		
		final CyNetwork unionNetwork = dynet.getUnionNetwork();
		final List<CyNode> neighbours = unionNetwork.getNeighborList(analyzedNode, Type.ANY);
		while(neighbours.remove(analyzedNode));
		
		
		neighbourTable = new JTable();
		neighbourTable.setFillsViewportHeight(true);
		neighbourTable.setSelectionMode(DefaultListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		neighbourTable.setCellSelectionEnabled(true);
		
		//to allow copying data from the table
		neighbourTable.registerKeyboardAction(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
		        JTable table = (JTable)e.getSource();
				int[] rowsSelected=table.getSelectedRows();
		        int[] colsSelected=table.getSelectedColumns();
		        
		        StringBuffer sbf=new StringBuffer();
		        
		        for (int i = 0; i < rowsSelected.length; i++){
		        	for (int j = 0; j < colsSelected.length; j++){
		        		sbf.append(table.getValueAt(rowsSelected[i],colsSelected[j]));
		        		if (j < colsSelected.length - 1) sbf.append("\t");
		            }
		            sbf.append("\n");
		        }
		        StringSelection stsel  = new StringSelection(sbf.toString());
		        Clipboard systemClipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		        systemClipboard.setContents(stsel,stsel);
			}
		}, KeyStroke.getKeyStroke(KeyEvent.VK_C,ActionEvent.CTRL_MASK,false), JComponent.WHEN_FOCUSED);
		neighbourTable.setDefaultRenderer(Double.class, new DefaultTableCellRenderer(){
			{
				setHorizontalAlignment(SwingConstants.RIGHT);
			}
			@Override
			protected void setValue(Object value) {
				if (value != null){
					Double doubleValue = (Double) value;
					if (doubleValue > Double.NEGATIVE_INFINITY && doubleValue < Double.POSITIVE_INFINITY){
						setText(doubleValue.toString());
					}else{
						setText(NumberFormat.getInstance().format(value));
					}
				}else{
					setText("");
				}
			}
		});
		neighbourTable.setModel(new AbstractTableModel() {
			
			@Override
			public Object getValueAt(int rowIndex, int columnIndex) {
				CyNode neighbour = neighbours.get(rowIndex);
				
				if (columnIndex == 0){
					return unionNetwork.getRow(neighbour).get(CyNetwork.NAME, String.class);
				}else if ((columnIndex - 1) < networks.size()){
					CyNetwork network = networks.get(columnIndex - 1);
					String networkName = network.getRow(network).get(CyNetwork.NAME, String.class);
					return unionNetwork.getRow(neighbour).get(networkName + "_" + nodeProperty, Object.class);
				}else{
					return nodeVariationMap.get(neighbour);
				}
			}
			
			@Override
			public int getRowCount() {
				return neighbours.size();
			}
			
			@Override
			public int getColumnCount() {
				return networks.size() + 2;
			}

			@Override
			public Class<?> getColumnClass(int columnIndex) {
				Class<?> type = null;
				if (columnIndex == 0){
					type = String.class;
				}else if ((columnIndex - 1) < networks.size()){
					CyNetwork network = networks.get(columnIndex - 1);
					String networkName = network.getRow(network).get(CyNetwork.NAME, String.class);
					CyColumn column = unionNetwork.getDefaultNodeTable().getColumn(networkName + "_" + nodeProperty);
					if (column != null) type = column.getType();
				}else{
					type = Double.class;
				}
				
				if (type != null){
					return type;
				}else{
					return super.getColumnClass(columnIndex);
				}
			}
		});
		neighbourTable.setAutoCreateRowSorter(true);
		neighbourTable.getRowSorter().toggleSortOrder(0);
		
		JTableHeader neighbourTableHeader = neighbourTable.getTableHeader();
		neighbourTableHeader.getColumnModel().getColumn(0).setHeaderValue("Nodes");
		for (int i = 0; i < networks.size(); i++){
			CyNetwork network = networks.get(i);
			String networkName = network.getRow(network).get(CyNetwork.NAME, String.class);
			neighbourTableHeader.getColumnModel().getColumn(i + 1).setHeaderValue(networkName);
		}
		neighbourTableHeader.getColumnModel().getColumn(networks.size() + 1).setHeaderValue("Variance");
		
		
		
		
		currentNodeTable = new JTable();
		currentNodeTable.setFillsViewportHeight(true);
		currentNodeTable.setSelectionMode(DefaultListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		currentNodeTable.setCellSelectionEnabled(true);
		currentNodeTable.registerKeyboardAction(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
		        JTable table = (JTable)e.getSource();
				int[] rowsSelected=table.getSelectedRows();
		        int[] colsSelected=table.getSelectedColumns();
		        
		        StringBuffer sbf=new StringBuffer();
		        
		        for (int i = 0; i < rowsSelected.length; i++){
		        	for (int j = 0; j < colsSelected.length; j++){
		        		sbf.append(table.getValueAt(rowsSelected[i],colsSelected[j]));
		        		if (j < colsSelected.length - 1) sbf.append("\t");
		            }
		            sbf.append("\n");
		        }
		        StringSelection stsel  = new StringSelection(sbf.toString());
		        Clipboard systemClipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		        systemClipboard.setContents(stsel,stsel);
			}
		}, KeyStroke.getKeyStroke(KeyEvent.VK_C,ActionEvent.CTRL_MASK,false), JComponent.WHEN_FOCUSED);
		currentNodeTable.setDefaultRenderer(Double.class, new DefaultTableCellRenderer(){
			{
				setHorizontalAlignment(SwingConstants.RIGHT);
			}
			@Override
			protected void setValue(Object value) {
				if (value != null){
					Double doubleValue = (Double) value;
					if (doubleValue > Double.NEGATIVE_INFINITY && doubleValue < Double.POSITIVE_INFINITY){
						setText(doubleValue.toString());
					}else{
						setText(NumberFormat.getInstance().format(value));
					}
				}else{
					setText("");
				}
			}
		});
		currentNodeTable.setModel(new AbstractTableModel() {
			
			@Override
			public Object getValueAt(int rowIndex, int columnIndex) {
				if (columnIndex == 0){
					return unionNetwork.getRow(analyzedNode).get(CyNetwork.NAME, String.class);
				}else if ((columnIndex - 1) < networks.size()){
					CyNetwork network = networks.get(columnIndex - 1);
					String networkName = network.getRow(network).get(CyNetwork.NAME, String.class);
					return unionNetwork.getRow(analyzedNode).get(networkName + "_" + nodeProperty, Object.class);
				}else{
					return nodeVariationMap.get(analyzedNode);
				}
			}
			
			@Override
			public int getRowCount() {
				return 1;
			}
			
			@Override
			public int getColumnCount() {
				return networks.size() + 2;
			}

			@Override
			public Class<?> getColumnClass(int columnIndex) {
				Class<?> type = null;
				if (columnIndex == 0){
					type = String.class;
				}else if ((columnIndex - 1) < networks.size()){
					CyNetwork network = networks.get(columnIndex - 1);
					String networkName = network.getRow(network).get(CyNetwork.NAME, String.class);
					CyColumn column = unionNetwork.getDefaultNodeTable().getColumn(networkName + "_" + nodeProperty);
					if (column != null) type = column.getType();
				}else{
					type = Double.class;
				}
				
				if (type != null){
					return type;
				}else{
					return super.getColumnClass(columnIndex);
				}
			}
		});
		
		JTableHeader currentNodeTableHeader = currentNodeTable.getTableHeader();
		currentNodeTableHeader.getColumnModel().getColumn(0).setHeaderValue("Node");
		for (int i = 0; i < networks.size(); i++){
			CyNetwork network = networks.get(i);
			String networkName = network.getRow(network).get(CyNetwork.NAME, String.class);
			currentNodeTableHeader.getColumnModel().getColumn(i + 1).setHeaderValue(networkName);
		}
		currentNodeTableHeader.getColumnModel().getColumn(networks.size() + 1).setHeaderValue("Variance");
		
		
		
		
		
		setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{10, 25, 10, 0};
		gridBagLayout.rowHeights = new int[]{10, 40, 0, 20, 30, 20, 10, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		JLabel titleLabel = new JLabel("Node colour currently mapped to variance in \"" + nodeProperty + "\" property:");
		GridBagConstraints gbc_titleLabel = new GridBagConstraints();
		gbc_titleLabel.anchor = GridBagConstraints.NORTHWEST;
		gbc_titleLabel.insets = new Insets(0, 0, 5, 5);
		gbc_titleLabel.gridx = 1;
		gbc_titleLabel.gridy = 1;
		add(titleLabel, gbc_titleLabel);
		
		JLabel currentNodeLabel = new JLabel("Current node:");
		GridBagConstraints gbc_currentNodeLabel = new GridBagConstraints();
		gbc_currentNodeLabel.anchor = GridBagConstraints.NORTHWEST;
		gbc_currentNodeLabel.insets = new Insets(0, 0, 5, 5);
		gbc_currentNodeLabel.gridx = 1;
		gbc_currentNodeLabel.gridy = 2;
		add(currentNodeLabel, gbc_currentNodeLabel);
		
		JPanel currentNodeTablePanel = new JPanel();
		GridBagConstraints gbc_currentNodeTablePanel = new GridBagConstraints();
		gbc_currentNodeTablePanel.insets = new Insets(0, 0, 5, 5);
		gbc_currentNodeTablePanel.fill = GridBagConstraints.BOTH;
		gbc_currentNodeTablePanel.gridx = 1;
		gbc_currentNodeTablePanel.gridy = 3;
		add(currentNodeTablePanel, gbc_currentNodeTablePanel);
		currentNodeTablePanel.setLayout(new BorderLayout(0, 0));
		currentNodeTablePanel.add(currentNodeTableHeader, BorderLayout.NORTH);
		currentNodeTablePanel.add(currentNodeTable, BorderLayout.CENTER);
		
		JLabel firstNeighboursLabel = new JLabel("First neighbours:");
		GridBagConstraints gbc_firstNeighboursLabel = new GridBagConstraints();
		gbc_firstNeighboursLabel.anchor = GridBagConstraints.SOUTHWEST;
		gbc_firstNeighboursLabel.insets = new Insets(0, 0, 5, 5);
		gbc_firstNeighboursLabel.gridx = 1;
		gbc_firstNeighboursLabel.gridy = 4;
		add(firstNeighboursLabel, gbc_firstNeighboursLabel);
		
		JPanel neighbourTablePanel = new JPanel();
		GridBagConstraints gbc_neighbourTablePanel = new GridBagConstraints();
		gbc_neighbourTablePanel.insets = new Insets(0, 0, 5, 5);
		gbc_neighbourTablePanel.fill = GridBagConstraints.BOTH;
		gbc_neighbourTablePanel.gridx = 1;
		gbc_neighbourTablePanel.gridy = 5;
		add(neighbourTablePanel, gbc_neighbourTablePanel);
		neighbourTablePanel.setLayout(new BorderLayout(0, 0));
		neighbourTablePanel.add(neighbourTableHeader, BorderLayout.NORTH);
		neighbourTablePanel.add(neighbourTable, BorderLayout.CENTER);
	}
}
