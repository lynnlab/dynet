package eu.primes.dynet.internal.variance;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.cytoscape.model.CyEdge;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.model.View;
import org.cytoscape.view.presentation.property.BasicVisualLexicon;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;

import eu.primes.dynet.internal.DynamicNetwork;
import eu.primes.dynet.internal.DynamicNetwork.CustomEdge;

/**
 * This task calculates the variances in a particular edge attribute across multiple networks. It then maps the variance
 * to edge colour. Edges that are more 'varying' are given stronger red colour. 
 *  
 * 
 * @author Ivan Hendy Goenawan
 */


public class EdgeVarianceMapperTask extends AbstractTask{
	private MultiNetworkVariationPanel parent;
	private DynamicNetwork dynet;
	private List<CyNetwork> selectedNetworks;
	private String edgeProperty;
	
	private HashMap<CyEdge, Double> edgeVariationMap;
	private double minVariationAboveThreshold;
	private double maxVariationBelowThreshold;
	
	private static final Color lowColor = DynamicNetwork.DEFAULT_EDGE_COLOR;
	private static final Color highColor = Color.RED;
	
	
	public EdgeVarianceMapperTask(MultiNetworkVariationPanel parent, DynamicNetwork dynet, List<CyNetwork> selectedNetworks, String edgeProperty){
		this.parent = parent;
		this.dynet = dynet;
		this.selectedNetworks = selectedNetworks;
		this.edgeProperty = edgeProperty;
	}
	
	@Override
	public void run(TaskMonitor taskMonitor) throws Exception {
		taskMonitor.setTitle("DyNet Edge Variation Mapper");
		taskMonitor.setStatusMessage("Mapping edge variation to edge colour.");
		
		CyNetwork unionNetwork = dynet.getUnionNetwork();
		CyNetworkView unionNetworkView = dynet.getUnionNetworkView();
		
		//clearing previous highlighting. if selectedNetworks less than 2, clear all networks
		//otherwise, just clear networks that are not selected
		for (CyNetworkView memberNetworkView : dynet.getMemberNetworkViews()){
			if (!selectedNetworks.contains(memberNetworkView.getModel()) || selectedNetworks.size() < 2){
				for (View<CyEdge> edgeView : memberNetworkView.getEdgeViews()){
					edgeView.clearValueLock(BasicVisualLexicon.EDGE_STROKE_UNSELECTED_PAINT);
				}
				memberNetworkView.updateView();
			}
		}
		if (selectedNetworks.size() < 2){
			for (View<CyEdge> edgeView : unionNetworkView.getEdgeViews()){
				edgeView.clearValueLock(BasicVisualLexicon.EDGE_STROKE_UNSELECTED_PAINT);
			}
			unionNetworkView.updateView();
			
			return;  //no need to proceed with calculation if less than 2 networks are selected
		}
		
		
		calculateEdgeVariations();
		parent.setEdgeMapperResult(edgeVariationMap);
		
		findOutlierThresholds();
		
		
		for (CyEdge edge : unionNetwork.getEdgeList()){
			Color highlighting = getColor(edgeVariationMap.get(edge), minVariationAboveThreshold, maxVariationBelowThreshold);
			unionNetworkView.getEdgeView(edge).setLockedValue(BasicVisualLexicon.EDGE_STROKE_UNSELECTED_PAINT, highlighting);
			
			for (CustomEdge otherEdge : dynet.getCorrespondingEdges(edge, unionNetwork)){
				View<CyEdge> otherEdgeView = otherEdge.getEdgeView();
				if (otherEdgeView != null && selectedNetworks.contains(otherEdge.getNetwork())){
					otherEdgeView.setLockedValue(BasicVisualLexicon.EDGE_STROKE_UNSELECTED_PAINT, highlighting);
				}
			}
		}
		
		unionNetworkView.updateView();
		for (CyNetworkView otherNetworkView : dynet.getMemberNetworkViews()){
			if (selectedNetworks.contains(otherNetworkView.getModel())){
				otherNetworkView.updateView();
			}
		}
	}
	
	
	private void calculateEdgeVariations() throws Exception{
		edgeVariationMap = new HashMap<CyEdge, Double>();
		
		CyNetwork unionNetwork = dynet.getUnionNetwork();
		
		ArrayList<String> propertyNames = new ArrayList<String>();
		for (CyNetwork network : selectedNetworks){
			String propertyName = network.getRow(network).get(CyNetwork.NAME, String.class) + "_" + edgeProperty;
			if (unionNetwork.getDefaultEdgeTable().getColumn(propertyName) == null){
				throw new Exception("Please choose an attribute present in all networks.");
			}
			propertyNames.add(propertyName);
		}
		
		
		for (CyEdge edge : unionNetwork.getEdgeList()){
			ArrayList<Double> values = new ArrayList<Double>();
			for (String propertyName : propertyNames){
				Object value = (unionNetwork.getRow(edge).get(propertyName, Object.class));
				
				if (value == null){
					value = new Double(0);
				}else{
					Class<?> propertyType = unionNetwork.getDefaultEdgeTable().getColumn(propertyName).getType();
					
					if (propertyType == Boolean.class){
						if ((Boolean)value) value = new Double(1);
						else value = new Double(0);
					}else if (propertyType == String.class){
						try{
							//attempt to parse string attribute. Should we just ban string attribute altogether?
							value = Double.parseDouble((String)value);
						}catch (Exception e){
							value = new Double(0);
						}
					}else{
						value = new Double(value.toString());
					}
				}

				if ((Double)value < 0){
					throw new Exception("Please choose an attribute containing only non-negative values.");
				}
				
				values.add((Double)value);
			}
			
			double variance = VarianceCalculator.normalizedVariance(values);
			edgeVariationMap.put(edge, variance);
		}
	}
	
	
	private void findOutlierThresholds(){
		Collection<Double> values = edgeVariationMap.values();
		double mean = average(values);
		double stdDev = stdDev(values, mean);
		double variationLowThreshold = mean - (2 * stdDev);
		double variationHighThreshold = mean + (2 * stdDev);
		
		minVariationAboveThreshold = Double.POSITIVE_INFINITY;
		maxVariationBelowThreshold = 0;
		for (double value : values){
			if (value < minVariationAboveThreshold && value > variationLowThreshold){
				minVariationAboveThreshold = value;
			}
			if (value > maxVariationBelowThreshold && value < variationHighThreshold){
				maxVariationBelowThreshold = value;
			}
		}
	}
	
	
	
	private static double stdDev(Collection<Double> values, double average){
		double sum = 0;
		
		for (Double value : values){
			sum += (value - average) * (value - average);
		}
		
		if (values.size() == 0) return 0;
		return Math.sqrt(sum / (values.size() - 1));
	}
	
	private static double average(Collection<Double> values){
		double sum = 0;
		
		for (Double value : values){
			sum += value;
		}
		
		if (values.size() == 0) return 0;
		return sum/values.size();
	}
	
	
	private static Color getColor(double value, double lowThreshold, double highThreshold){
		double fractionDistance = rescale(value, lowThreshold, highThreshold, 0, 1);
		
		int r = (int) Math.round(fractionBetweenTwoNumbers(lowColor.getRed(), highColor.getRed(), fractionDistance));
		int g = (int) Math.round(fractionBetweenTwoNumbers(lowColor.getGreen(), highColor.getGreen(), fractionDistance));
		int b = (int) Math.round(fractionBetweenTwoNumbers(lowColor.getBlue(), highColor.getBlue(), fractionDistance));
		
		return new Color(r,g,b);
	}
	
	
	private static double rescale(double value, double srcLow, double srcHigh, double destLow, double destHigh){
		if (value < srcLow) return destLow;
		if (value > srcHigh) return destHigh;
		
		double srcInterval = srcHigh - srcLow;
		if (srcInterval == 0) return (destLow + destHigh) / 2;
		
		double fraction = (value - srcLow) / srcInterval;
		return destLow + fraction * (destHigh - destLow);
	}
	
	
	//calculate points between two numbers that are a 'fraction' of the total distance
	//e.g. The point that is 0.6 of the distance between 11 and 15 = 11 + (0.6 * 4) = 13.4
	private static double fractionBetweenTwoNumbers(double start, double stop, double fraction){
		double interval = Math.abs(stop - start);
		double step = fraction * interval;
		
		if (stop > start) return start + step;
		else if (stop < start) return start - step;
		else return start;
	}
}
