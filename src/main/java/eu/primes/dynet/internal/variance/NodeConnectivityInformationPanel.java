package eu.primes.dynet.internal.variance;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.DefaultListSelectionModel;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;

import org.cytoscape.model.CyColumn;
import org.cytoscape.model.CyEdge;
import org.cytoscape.model.CyEdge.Type;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;

import eu.primes.dynet.internal.DynamicNetwork;

/**
 * This panel displays information about the node colour mapping scheme (focusing on a particular node) that has been 
 * applied by NodeVarianceMapperTask (Multi-network variation analysis) if the user has chosen to compare the node's 
 * connectivity. It displays two tables. One table shows the edges that are adjacent to the node being analyzed and their
 * original values in all the networks that are included in the analysis. The other table contains the currently analyzed
 * nodes along with its direct neighbours. It also shows the calculated 'connectivity variance' of those nodes.
 * 
 * @author Ivan Hendy Goenawan
 */

public class NodeConnectivityInformationPanel extends JPanel{
	
	private DynamicNetwork dynet;
	private List<CyNetwork> networks;
	private String edgeProperty;
	private Map<CyNode, Double> nodeVariationMap;
	private CyNode analyzedNode;
	
	private JTable variancesTable;
	private JTable edgesTable;
	JLabel propertyNameLabel;

	public NodeConnectivityInformationPanel(DynamicNetwork dynet, final List<CyNetwork> networks, final String edgeProperty, final Map<CyNode, Double> nodeVariationMap, final CyNode analyzedNode){
		this.dynet = dynet;
		this.networks = networks;
		this.edgeProperty = edgeProperty;
		this.analyzedNode = analyzedNode;
		this.nodeVariationMap =  nodeVariationMap;
		
		
		propertyNameLabel = new JLabel(edgeProperty);
		
		
		final CyNetwork unionNetwork = dynet.getUnionNetwork();
		final ArrayList<CyNode> nodes = new ArrayList<CyNode>();
		nodes.add(analyzedNode);
		for (CyNode neighbour : unionNetwork.getNeighborList(analyzedNode, Type.ANY)){
			if (neighbour != analyzedNode) nodes.add(neighbour);
		}
		
		
		
		variancesTable = new JTable();
		variancesTable.setFillsViewportHeight(true);
		variancesTable.setSelectionMode(DefaultListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		variancesTable.setCellSelectionEnabled(true);
		
		//this is to allow copying data from the table
		variancesTable.registerKeyboardAction(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
		        JTable table = (JTable)e.getSource();
				int[] rowsSelected=table.getSelectedRows();
		        int[] colsSelected=table.getSelectedColumns();
		        
		        StringBuffer sbf=new StringBuffer();
		        
		        for (int i = 0; i < rowsSelected.length; i++){
		        	for (int j = 0; j < colsSelected.length; j++){
		        		sbf.append(table.getValueAt(rowsSelected[i],colsSelected[j]));
		        		if (j < colsSelected.length - 1) sbf.append("\t");
		            }
		            sbf.append("\n");
		        }
		        StringSelection stsel  = new StringSelection(sbf.toString());
		        Clipboard systemClipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		        systemClipboard.setContents(stsel,stsel);
			}
		}, KeyStroke.getKeyStroke(KeyEvent.VK_C,ActionEvent.CTRL_MASK,false), JComponent.WHEN_FOCUSED);
		variancesTable.setDefaultRenderer(Double.class, new DefaultTableCellRenderer(){
			{
				setHorizontalAlignment(SwingConstants.RIGHT);
			}
			@Override
			protected void setValue(Object value) {
				if (value != null){
					Double doubleValue = (Double) value;
					if (doubleValue > Double.NEGATIVE_INFINITY && doubleValue < Double.POSITIVE_INFINITY){
						setText(doubleValue.toString());
					}else{
						setText(NumberFormat.getInstance().format(value));
					}
				}else{
					setText("");
				}
			}
		});
		variancesTable.setModel(new AbstractTableModel() {
			
			@Override
			public Object getValueAt(int rowIndex, int columnIndex) {
				CyNode node = nodes.get(rowIndex);
				
				if (columnIndex == 0){
					return unionNetwork.getRow(node).get(CyNetwork.NAME, String.class);
				}else{
					return nodeVariationMap.get(node);
				}
			}
			
			@Override
			public int getRowCount() {
				return nodes.size();
			}
			
			@Override
			public int getColumnCount() {
				return 2;
			}

			@Override
			public Class<?> getColumnClass(int columnIndex) {
				if (columnIndex == 0){
					return String.class;
				}else if (columnIndex == 1){
					return Double.class;
				}else{
					return super.getColumnClass(columnIndex);
				}
			}
		});
		variancesTable.setAutoCreateRowSorter(true);
		variancesTable.getRowSorter().toggleSortOrder(0);
		
		JTableHeader variancesTableHeader = variancesTable.getTableHeader();
		variancesTableHeader.getColumnModel().getColumn(0).setHeaderValue("Nodes");
		variancesTableHeader.getColumnModel().getColumn(1).setHeaderValue("Variance");
		
		
		
		final List<CyEdge> adjacentEdges = unionNetwork.getAdjacentEdgeList(analyzedNode, Type.ANY);
		
		
		edgesTable = new JTable();
		edgesTable.setFillsViewportHeight(true);
		edgesTable.setSelectionMode(DefaultListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		edgesTable.setCellSelectionEnabled(true);
		
		//this is to allow copying data from the table
		edgesTable.registerKeyboardAction(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
		        JTable table = (JTable)e.getSource();
				int[] rowsSelected=table.getSelectedRows();
		        int[] colsSelected=table.getSelectedColumns();
		        
		        StringBuffer sbf=new StringBuffer();
		        
		        for (int i = 0; i < rowsSelected.length; i++){
		        	for (int j = 0; j < colsSelected.length; j++){
		        		sbf.append(table.getValueAt(rowsSelected[i],colsSelected[j]));
		        		if (j < colsSelected.length - 1) sbf.append("\t");
		            }
		            sbf.append("\n");
		        }
		        StringSelection stsel  = new StringSelection(sbf.toString());
		        Clipboard systemClipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		        systemClipboard.setContents(stsel,stsel);
			}
		}, KeyStroke.getKeyStroke(KeyEvent.VK_C,ActionEvent.CTRL_MASK,false), JComponent.WHEN_FOCUSED);
		edgesTable.setDefaultRenderer(Double.class, new DefaultTableCellRenderer(){
			{
				setHorizontalAlignment(SwingConstants.RIGHT);
			}
			@Override
			protected void setValue(Object value) {
				if (value != null){
					Double doubleValue = (Double) value;
					if (doubleValue > Double.NEGATIVE_INFINITY && doubleValue < Double.POSITIVE_INFINITY){
						setText(doubleValue.toString());
					}else{
						setText(NumberFormat.getInstance().format(value));
					}
				}else{
					setText("");
				}
			}
		});
		edgesTable.setModel(new AbstractTableModel() {
			
			@Override
			public Object getValueAt(int rowIndex, int columnIndex) {
				CyEdge edge = adjacentEdges.get(rowIndex);
				CyNode source = edge.getSource();
				CyNode target = edge.getTarget();
				CyNode neighbor = (source == analyzedNode) ? target : source;
				
				if (columnIndex == 0){
					return unionNetwork.getRow(neighbor).get(CyNetwork.NAME, String.class);
				}else if ((columnIndex - 1) < networks.size()){
					CyNetwork network = networks.get(columnIndex - 1);
					String networkName = network.getRow(network).get(CyNetwork.NAME, String.class);
					return unionNetwork.getRow(edge).get(networkName + "_" + edgeProperty, Object.class);
				}else{
					return null;
				}
			}
			
			@Override
			public int getRowCount() {
				return adjacentEdges.size();
			}
			
			@Override
			public int getColumnCount() {
				return networks.size() + 1;
			}

			@Override
			public Class<?> getColumnClass(int columnIndex) {
				Class<?> type = null;
				if (columnIndex == 0){
					type = String.class;
				}else if ((columnIndex - 1) < networks.size()){
					CyNetwork network = networks.get(columnIndex - 1);
					String networkName = network.getRow(network).get(CyNetwork.NAME, String.class);
					CyColumn column = unionNetwork.getDefaultEdgeTable().getColumn(networkName + "_" + edgeProperty);
					if (column != null) type = column.getType();
				}
				
				if (type != null){
					return type;
				}else{
					return super.getColumnClass(columnIndex);
				}
			}
		});
		edgesTable.setAutoCreateRowSorter(true);
		edgesTable.getRowSorter().toggleSortOrder(0);
		
		JTableHeader edgesTableHeader = edgesTable.getTableHeader();
		edgesTableHeader.getColumnModel().getColumn(0).setHeaderValue("Edge to");
		for (int i = 0; i < networks.size(); i++){
			CyNetwork network = networks.get(i);
			String networkName = network.getRow(network).get(CyNetwork.NAME, String.class);
			edgesTableHeader.getColumnModel().getColumn(i + 1).setHeaderValue(networkName);
		}
		
		
		
		
		setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{10, 140, 0, 10, 0};
		gridBagLayout.rowHeights = new int[]{10, 25, 40, 0, 20, 30, 20, 10, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		JLabel titleLabel = new JLabel("Node colour currently mapped to variance in their connectivity:");
		GridBagConstraints gbc_titleLabel = new GridBagConstraints();
		gbc_titleLabel.gridwidth = 2;
		gbc_titleLabel.anchor = GridBagConstraints.NORTHWEST;
		gbc_titleLabel.insets = new Insets(0, 0, 5, 5);
		gbc_titleLabel.gridx = 1;
		gbc_titleLabel.gridy = 1;
		add(titleLabel, gbc_titleLabel);
		
		JLabel edgeWeightAttributeLabel = new JLabel("Edge weight attribute:");
		GridBagConstraints gbc_edgeWeightAttributeLabel = new GridBagConstraints();
		gbc_edgeWeightAttributeLabel.anchor = GridBagConstraints.NORTHWEST;
		gbc_edgeWeightAttributeLabel.insets = new Insets(0, 0, 5, 5);
		gbc_edgeWeightAttributeLabel.gridx = 1;
		gbc_edgeWeightAttributeLabel.gridy = 2;
		add(edgeWeightAttributeLabel, gbc_edgeWeightAttributeLabel);
		
		GridBagConstraints gbc_propertyNameLabel = new GridBagConstraints();
		gbc_propertyNameLabel.anchor = GridBagConstraints.NORTHWEST;
		gbc_propertyNameLabel.insets = new Insets(0, 0, 5, 5);
		gbc_propertyNameLabel.gridx = 2;
		gbc_propertyNameLabel.gridy = 2;
		add(propertyNameLabel, gbc_propertyNameLabel);
		
		JLabel edgesOfCurrentNodeLabel = new JLabel("Edges of current node:");
		GridBagConstraints gbc_edgesOfCurrentNodeLabel = new GridBagConstraints();
		gbc_edgesOfCurrentNodeLabel.gridwidth = 2;
		gbc_edgesOfCurrentNodeLabel.anchor = GridBagConstraints.NORTHWEST;
		gbc_edgesOfCurrentNodeLabel.insets = new Insets(0, 0, 5, 5);
		gbc_edgesOfCurrentNodeLabel.gridx = 1;
		gbc_edgesOfCurrentNodeLabel.gridy = 3;
		add(edgesOfCurrentNodeLabel, gbc_edgesOfCurrentNodeLabel);
		
		JPanel edgesTablePanel = new JPanel();
		GridBagConstraints gbc_edgesTablePanel = new GridBagConstraints();
		gbc_edgesTablePanel.gridwidth = 2;
		gbc_edgesTablePanel.insets = new Insets(0, 0, 5, 5);
		gbc_edgesTablePanel.fill = GridBagConstraints.BOTH;
		gbc_edgesTablePanel.gridx = 1;
		gbc_edgesTablePanel.gridy = 4;
		add(edgesTablePanel, gbc_edgesTablePanel);
		edgesTablePanel.setLayout(new BorderLayout(0, 0));
		edgesTablePanel.add(edgesTableHeader, BorderLayout.NORTH);
		edgesTablePanel.add(edgesTable, BorderLayout.CENTER);
		
		JLabel varianceListLabel = new JLabel("Current node and first neighbours' variance scores:");
		GridBagConstraints gbc_varianceListLabel = new GridBagConstraints();
		gbc_varianceListLabel.gridwidth = 2;
		gbc_varianceListLabel.anchor = GridBagConstraints.SOUTHWEST;
		gbc_varianceListLabel.insets = new Insets(0, 0, 5, 5);
		gbc_varianceListLabel.gridx = 1;
		gbc_varianceListLabel.gridy = 5;
		add(varianceListLabel, gbc_varianceListLabel);
		
		JPanel variancesTablePanel = new JPanel();
		GridBagConstraints gbc_variancesTablePanel = new GridBagConstraints();
		gbc_variancesTablePanel.gridwidth = 2;
		gbc_variancesTablePanel.insets = new Insets(0, 0, 5, 5);
		gbc_variancesTablePanel.fill = GridBagConstraints.BOTH;
		gbc_variancesTablePanel.gridx = 1;
		gbc_variancesTablePanel.gridy = 6;
		add(variancesTablePanel, gbc_variancesTablePanel);
		variancesTablePanel.setLayout(new BorderLayout(0, 0));
		variancesTablePanel.add(variancesTableHeader, BorderLayout.NORTH);
		variancesTablePanel.add(variancesTable, BorderLayout.CENTER);
	}
}
