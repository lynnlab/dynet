package eu.primes.dynet.internal;

import java.awt.Paint;
import java.util.List;

import org.cytoscape.model.CyEdge;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.model.View;
import org.cytoscape.view.presentation.property.BasicVisualLexicon;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;

import eu.primes.dynet.internal.DynamicNetwork.CustomEdge;
import eu.primes.dynet.internal.DynamicNetwork.CustomNode;

/**
 * A task to resync member networks. The current state of the unionNetwork will be copied into the member networks (not the other way around)
 * 
 * @author Ivan Hendy Goenawan
 */
public class ResyncNetworkViewsTask extends AbstractTask{

	private DynamicNetwork dynet;
	private List<CyNetworkView> networkViews;
	private boolean syncPanZoom;
	private boolean syncLocations;
	private boolean syncSelections;
	
	public ResyncNetworkViewsTask(DynamicNetwork dynet, List<CyNetworkView> networkViews, boolean syncPanZoom, 
			boolean syncLocations, boolean syncSelections){
		
		this.dynet = dynet;
		this.networkViews = networkViews;
		this.syncPanZoom = syncPanZoom;
		this.syncLocations = syncLocations;
		this.syncSelections = syncSelections;
	}
	
	@Override
	public void run(TaskMonitor taskMonitor) throws Exception {
		taskMonitor.setTitle("Dynet network view synchroniser");
		taskMonitor.setStatusMessage("Synchronizing network views");
		
		CyNetwork unionNetwork = dynet.getUnionNetwork();
		CyNetworkView unionNetworkView = dynet.getUnionNetworkView();
		
		
		List<CyEdge> allEdges = unionNetwork.getEdgeList();
		for (CyEdge edge : allEdges){
			View<CyEdge> edgeView = unionNetworkView.getEdgeView(edge);
			
			for (CustomEdge correspondingEdge : dynet.getCorrespondingEdges(edge, unionNetwork)){
				CyNetworkView correspondingNetworkView = correspondingEdge.getNetworkView();
				View<CyEdge> correspondingEdgeView = correspondingEdge.getEdgeView();
				if (networkViews.contains(correspondingNetworkView) && correspondingEdgeView != null){
					
					if (syncSelections){
						Boolean selected = unionNetwork.getRow(edge).get(CyNetwork.SELECTED, Boolean.class);
						correspondingEdge.getTableRow().set(CyNetwork.SELECTED, selected);
					}
					
				}
			}
		}
		
		
		List<CyNode> allNodes = unionNetwork.getNodeList();
		
		for (CyNode node : allNodes){
			View<CyNode> nodeView = unionNetworkView.getNodeView(node);

			for (CustomNode correspondingNode : dynet.getCorrespondingNodes(node, unionNetwork)){
				CyNetworkView correspondingNetworkView = correspondingNode.getNetworkView();
				View<CyNode> correspondingNodeView = correspondingNode.getNodeView();
				if (networkViews.contains(correspondingNetworkView) && correspondingNodeView != null){
					if (syncLocations){
						double x = nodeView.getVisualProperty(BasicVisualLexicon.NODE_X_LOCATION);
						double y = nodeView.getVisualProperty(BasicVisualLexicon.NODE_Y_LOCATION);
						correspondingNodeView.setVisualProperty(BasicVisualLexicon.NODE_X_LOCATION, x);
						correspondingNodeView.setVisualProperty(BasicVisualLexicon.NODE_Y_LOCATION, y);
					}
					
					
					if (syncSelections){
						Boolean selected = unionNetwork.getRow(node).get(CyNetwork.SELECTED, Boolean.class);
						correspondingNode.getTableRow().set(CyNetwork.SELECTED, selected);
					}
				}
			}
		}
		
		
		for (CyNetworkView networkView : networkViews){
			if (syncPanZoom){
				networkView.setVisualProperty(BasicVisualLexicon.NETWORK_CENTER_X_LOCATION, unionNetworkView.getVisualProperty(BasicVisualLexicon.NETWORK_CENTER_X_LOCATION));
				networkView.setVisualProperty(BasicVisualLexicon.NETWORK_CENTER_Y_LOCATION, unionNetworkView.getVisualProperty(BasicVisualLexicon.NETWORK_CENTER_Y_LOCATION));
				networkView.setVisualProperty(BasicVisualLexicon.NETWORK_SCALE_FACTOR, unionNetworkView.getVisualProperty(BasicVisualLexicon.NETWORK_SCALE_FACTOR));
			}
			networkView.updateView();
		}
	}

}
