package eu.primes.dynet.internal.nodeanalyzer;

import java.util.List;

import org.cytoscape.app.CyAppAdapter;
import org.cytoscape.model.CyEdge;
import org.cytoscape.model.CyEdge.Type;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.model.View;
import org.cytoscape.view.presentation.property.BasicVisualLexicon;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;

import eu.primes.dynet.internal.ControlPanel;
import eu.primes.dynet.internal.DynamicNetwork;
import eu.primes.dynet.internal.DynamicNetwork.CustomEdge;
import eu.primes.dynet.internal.DynamicNetwork.CustomNode;

/**
 * This task sets up the Node Analyzer mode. In the node analyzer mode, only the currently analyzed node and its directly
 * adjacent neighbours are fully visible, while the other nodes and edges are made transparent, making it easy to analyze
 * a single node. The control panel will also be replaced with a panel containing information about the current colour mapping
 * applied to both the nodes and the edges.
 * 
 * @author Ivan Hendy Goenawan
 */

public class NodeAnalyzerTask extends AbstractTask{
	private View<CyNode> nodeView;
	private DynamicNetwork dynet;
	private CyAppAdapter appAdapter;
	private ControlPanel controlPanel;
	
	private int transparency = 40;
	
	
	public NodeAnalyzerTask(View<CyNode> nodeView, DynamicNetwork dynet, CyAppAdapter appAdapter, ControlPanel controlPanel){
		this.nodeView = nodeView;
		this.dynet = dynet;
		this.appAdapter = appAdapter;
		this.controlPanel = controlPanel;
	}
	
	@Override
	public void run(TaskMonitor taskMonitor) throws Exception {
		NodeAnalyzerPanel nodeAnalyzerPanel = new NodeAnalyzerPanel(nodeView.getModel(), dynet, appAdapter, controlPanel);
		controlPanel.replaceMainPanel(nodeAnalyzerPanel);
		
		updateNetworkView();
	}
	
	
	private void updateNetworkView(){
		
		CyNetwork unionNetwork = dynet.getUnionNetwork();
		CyNetworkView unionNetworkView = dynet.getUnionNetworkView();
		
		//make all nodes and edges in union network transparent
		for (View<CyNode> unionNetworkNodeView : unionNetworkView.getNodeViews()){
			unionNetworkNodeView.setLockedValue(BasicVisualLexicon.NODE_TRANSPARENCY, transparency);
			unionNetworkNodeView.setLockedValue(BasicVisualLexicon.NODE_BORDER_TRANSPARENCY, transparency);
		}
		
		for (View<CyEdge> unionNetworkEdgeView : unionNetworkView.getEdgeViews()){
			unionNetworkEdgeView.setLockedValue(BasicVisualLexicon.EDGE_TRANSPARENCY, transparency);
		}
		
		
		//make all nodes and edges in member networs transparent
		for (CyNetworkView memberNetworkView : dynet.getMemberNetworkViews()){
			for (View<CyNode> memberNetworkNodeView : memberNetworkView.getNodeViews()){
				memberNetworkNodeView.setLockedValue(BasicVisualLexicon.NODE_TRANSPARENCY, transparency);
				memberNetworkNodeView.setLockedValue(BasicVisualLexicon.NODE_BORDER_TRANSPARENCY, transparency);
			}
			
			for (View<CyEdge> memberNetworkEdgeView : memberNetworkView.getEdgeViews()){
				memberNetworkEdgeView.setLockedValue(BasicVisualLexicon.EDGE_TRANSPARENCY, transparency);
			}
		}
		
		
		
		//Make current node opaque. We don't need to worry about the current node being filtered in
		//some networks. If it's invisible, it's invisible.
		nodeView.setLockedValue(BasicVisualLexicon.NODE_TRANSPARENCY, 255);
		nodeView.setLockedValue(BasicVisualLexicon.NODE_BORDER_TRANSPARENCY, 255);
		
		//if all edges connecting a neighbour to the current node are invisible (filtered), 
		//don't make the node opaque, keep it transparent 
		for (CyNode neighbour : unionNetwork.getNeighborList(nodeView.getModel(), Type.ANY)){
			boolean unfilteredEdgeExist = false;
			for (CyEdge edge : unionNetwork.getConnectingEdgeList(nodeView.getModel(), neighbour, Type.ANY)){
				View<CyEdge> edgeView = unionNetworkView.getEdgeView(edge);
				if (edgeView.getVisualProperty(BasicVisualLexicon.EDGE_VISIBLE)){
					unfilteredEdgeExist = true;
					break;
				}
			}
			if (unfilteredEdgeExist){
				View<CyNode> neighborNodeView = unionNetworkView.getNodeView(neighbour);
				neighborNodeView.setLockedValue(BasicVisualLexicon.NODE_TRANSPARENCY, 255);
				neighborNodeView.setLockedValue(BasicVisualLexicon.NODE_BORDER_TRANSPARENCY, 255);
			}
		}
		
		
		
		//now do the same in other networks independently
		for (CustomNode correspondingNode : dynet.getCorrespondingNodes(nodeView.getModel(), unionNetwork)){
			CyNetwork network = correspondingNode.getNetwork();
			CyNode node = correspondingNode.getNode();
			CyNetworkView networkView = correspondingNode.getNetworkView();
			if (networkView == null) continue;
			
			networkView.getNodeView(node).setLockedValue(BasicVisualLexicon.NODE_TRANSPARENCY, 255);
			networkView.getNodeView(node).setLockedValue(BasicVisualLexicon.NODE_BORDER_TRANSPARENCY, 255);
			
			for (CyNode neighbour : network.getNeighborList(node, Type.ANY)){
				boolean unfilteredEdgeExist = false;
				for (CyEdge edge : network.getConnectingEdgeList(node, neighbour, Type.ANY)){
					View<CyEdge> edgeView = networkView.getEdgeView(edge);
					if (edgeView.getVisualProperty(BasicVisualLexicon.EDGE_VISIBLE)){
						unfilteredEdgeExist = true;
						break;
					}
				}
				if (unfilteredEdgeExist){
					View<CyNode> neighborNodeView = networkView.getNodeView(neighbour);
					neighborNodeView.setLockedValue(BasicVisualLexicon.NODE_TRANSPARENCY, 255);
					neighborNodeView.setLockedValue(BasicVisualLexicon.NODE_BORDER_TRANSPARENCY, 255);
				}
			}
		}
		
		
		
		
		//for edges, we don't need to care about filtering in each network. If it's invisible, it's invisible.
		List<CyEdge> adjacentEdges = unionNetwork.getAdjacentEdgeList(nodeView.getModel(), Type.ANY);
		for (CyEdge adjacentEdge : adjacentEdges){		
			View<CyEdge> adjacentEdgeView = unionNetworkView.getEdgeView(adjacentEdge);
			adjacentEdgeView.setLockedValue(BasicVisualLexicon.EDGE_TRANSPARENCY, 255);
			for (CustomEdge correspondingAdjacent : dynet.getCorrespondingEdges(adjacentEdge, unionNetwork)){
				View<CyEdge> correspondingAdjacentView = correspondingAdjacent.getEdgeView();
				if (correspondingAdjacentView != null){
					correspondingAdjacentView.setLockedValue(BasicVisualLexicon.EDGE_TRANSPARENCY, 255);
				}
			}
		}
		
		
		unionNetworkView.updateView();
		for (CyNetworkView memberNetworkView : dynet.getMemberNetworkViews()){
			memberNetworkView.updateView();
		}
	}	
}
