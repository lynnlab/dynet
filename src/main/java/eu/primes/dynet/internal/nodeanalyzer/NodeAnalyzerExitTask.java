package eu.primes.dynet.internal.nodeanalyzer;

import org.cytoscape.app.CyAppAdapter;
import org.cytoscape.model.CyEdge;
import org.cytoscape.model.CyNode;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.model.View;
import org.cytoscape.view.presentation.property.BasicVisualLexicon;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;

import eu.primes.dynet.internal.ControlPanel;
import eu.primes.dynet.internal.DynamicNetwork;

/**
 * This task is run when user exits from the NodeAnalyzer mode. It resets the ControlPanel so that it shows the normal
 * menus again, and it clears the transparency locks that have been previously applied.
 * 
 * @author Ivan Hendy Goenawan
 */

public class NodeAnalyzerExitTask extends AbstractTask{

	private DynamicNetwork dynet;
	private ControlPanel controlPanel;
	private CyAppAdapter appAdapter;
	
	public NodeAnalyzerExitTask(DynamicNetwork dynet, ControlPanel controlPanel, CyAppAdapter appAdapter){
		this.dynet = dynet;
		this.controlPanel = controlPanel;
		this.appAdapter = appAdapter;
	}


	@Override
	public void run(TaskMonitor taskMonitor) throws Exception {
		
		for (View<CyNode> nodeView : dynet.getUnionNetworkView().getNodeViews()){
			nodeView.clearValueLock(BasicVisualLexicon.NODE_TRANSPARENCY);
			nodeView.clearValueLock(BasicVisualLexicon.NODE_BORDER_TRANSPARENCY);
		}
		
		for (View<CyEdge> edgeView : dynet.getUnionNetworkView().getEdgeViews()){
			edgeView.clearValueLock(BasicVisualLexicon.EDGE_TRANSPARENCY);
		}
		
		dynet.getUnionNetworkView().updateView();
		
		for (CyNetworkView memberNetworkView : dynet.getMemberNetworkViews()){
			for (View<CyNode> memberNetworkNodeView : memberNetworkView.getNodeViews()){
				memberNetworkNodeView.clearValueLock(BasicVisualLexicon.NODE_TRANSPARENCY);
				memberNetworkNodeView.clearValueLock(BasicVisualLexicon.NODE_BORDER_TRANSPARENCY);
			}
			
			for (View<CyEdge> memberNetworkEdgeView : memberNetworkView.getEdgeViews()){
				memberNetworkEdgeView.clearValueLock(BasicVisualLexicon.EDGE_TRANSPARENCY);
			}
			
			memberNetworkView.updateView();
		}
		
		controlPanel.resetMainPanel();
		controlPanel.updateView();
	}
}
