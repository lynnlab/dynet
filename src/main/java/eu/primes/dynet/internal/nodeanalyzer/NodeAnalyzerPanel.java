package eu.primes.dynet.internal.nodeanalyzer;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.cytoscape.app.CyAppAdapter;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;
import org.cytoscape.work.TaskIterator;

import eu.primes.dynet.internal.ControlPanel;
import eu.primes.dynet.internal.DynamicNetwork;

/**
 * This panel will replace the main control panel when a user enters the Node Analyzer node. This panel provides information
 * about the colour mapping scheme that is currently applied to both the nodes and the edges. The panel requests for this
 * information from the ControlPanel which provides the information in the form of a JPanel which will then be added as a
 * child component. 
 * 
 * @author Ivan Hendy Goenawan
 */

public class NodeAnalyzerPanel extends JPanel {
	private CyNode analyzedNode;
	private DynamicNetwork dynet;
	private CyAppAdapter appAdapter;
	private ControlPanel controlPanel;
	
	private JPanel topPanel;
	private JButton exitButton;
	private JLabel nodeNameLabel;
	
	
	public NodeAnalyzerPanel(CyNode analyzedNode, DynamicNetwork dynet, CyAppAdapter appAdapter, ControlPanel controlPanel){
		this.analyzedNode = analyzedNode;
		this.dynet = dynet;
		this.appAdapter = appAdapter;
		this.controlPanel = controlPanel;
		
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		
		nodeNameLabel = new JLabel();
		String nodeName = dynet.getUnionNetwork().getRow(analyzedNode).get(CyNetwork.NAME, String.class);
		nodeNameLabel.setText(nodeName);
		
		
		exitButton = new JButton("Exit Node Analyzer");
		exitButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				NodeAnalyzerPanel.this.appAdapter.getTaskManager().execute(new TaskIterator(
					new NodeAnalyzerExitTask(NodeAnalyzerPanel.this.dynet, NodeAnalyzerPanel.this.controlPanel, NodeAnalyzerPanel.this.appAdapter)));
			}
		});
		
		
		topPanel = new JPanel();
		GridBagLayout gbl_topPanel = new GridBagLayout();
		gbl_topPanel.columnWidths = new int[]{10, 80, 140, 0, 10, 0};
		gbl_topPanel.rowHeights = new int[]{10, 0, 10, 0};
		gbl_topPanel.columnWeights = new double[]{0.0, 0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
		gbl_topPanel.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		topPanel.setLayout(gbl_topPanel);
		
		
		JLabel currentNodeLabel = new JLabel("Current node:");
		GridBagConstraints gbc_currentNodeLabel = new GridBagConstraints();
		gbc_currentNodeLabel.anchor = GridBagConstraints.WEST;
		gbc_currentNodeLabel.insets = new Insets(0, 0, 5, 5);
		gbc_currentNodeLabel.gridx = 1;
		gbc_currentNodeLabel.gridy = 1;
		topPanel.add(currentNodeLabel, gbc_currentNodeLabel);
		
		
		GridBagConstraints gbc_nodeNameLabel = new GridBagConstraints();
		gbc_nodeNameLabel.anchor = GridBagConstraints.WEST;
		gbc_nodeNameLabel.insets = new Insets(0, 0, 5, 5);
		gbc_nodeNameLabel.gridx = 2;
		gbc_nodeNameLabel.gridy = 1;
		topPanel.add(nodeNameLabel, gbc_nodeNameLabel);
		
		GridBagConstraints gbc_exitButton = new GridBagConstraints();
		gbc_exitButton.anchor = GridBagConstraints.NORTHWEST;
		gbc_exitButton.insets = new Insets(0, 0, 5, 5);
		gbc_exitButton.gridx = 3;
		gbc_exitButton.gridy = 1;
		topPanel.add(exitButton, gbc_exitButton);
		
		
		
		topPanel.setMaximumSize(new Dimension(Integer.MAX_VALUE, topPanel.getPreferredSize().height));
		add(topPanel);
		
		//contains information about the current colour mapping applied to nodes
		JPanel nodeInformationPanel = controlPanel.getNodeInformationPanel(analyzedNode);
		if (nodeInformationPanel != null){
			nodeInformationPanel.setMaximumSize(new Dimension(Integer.MAX_VALUE, nodeInformationPanel.getPreferredSize().height));
			add(nodeInformationPanel);
		}else{
			add(new JPanel(){
				{
					setLayout(new FlowLayout(FlowLayout.LEFT));
					setBorder(new EmptyBorder(5, 5, 5, 5));
					add(new JLabel("Node colour mapping currently disabled."));
					setMaximumSize(new Dimension(Integer.MAX_VALUE, getPreferredSize().height));
				}
			});
		}
		
		
		add(Box.createRigidArea(new Dimension(0, 20)));
		
		
		//contains the information about the current colour mapping applied to edges
		JPanel edgeInformationPanel = controlPanel.getEdgeInformationPanel(analyzedNode);
		if (edgeInformationPanel != null){
			edgeInformationPanel.setMaximumSize(new Dimension(Integer.MAX_VALUE, edgeInformationPanel.getPreferredSize().height));
			add(edgeInformationPanel);
		}else{
			add(new JPanel(){
				{
					setLayout(new FlowLayout(FlowLayout.LEFT));
					setBorder(new EmptyBorder(5, 5, 5, 5));
					add(new JLabel("Edge colour mapping currently disabled."));
					setMaximumSize(new Dimension(Integer.MAX_VALUE, getPreferredSize().height));
				}
			});
		}
	}
}
