package eu.primes.dynet.internal.nodeanalyzer;

import org.cytoscape.app.CyAppAdapter;
import org.cytoscape.model.CyNode;
import org.cytoscape.task.AbstractNodeViewTaskFactory;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.model.View;
import org.cytoscape.work.TaskIterator;

import eu.primes.dynet.internal.ControlPanel;
import eu.primes.dynet.internal.DynamicNetwork;

/**
 * This is the task factory that adds a menu to enter the Node Analyzer mode when a user right-clicks on a node .
 * 
 * @author Ivan Hendy Goenawan
 */

public class NodeAnalyzerTaskFactory extends AbstractNodeViewTaskFactory{
	private DynamicNetwork dynet;
	private CyAppAdapter appAdapter;
	private ControlPanel controlPanel;
	
	public NodeAnalyzerTaskFactory(DynamicNetwork dynet, CyAppAdapter appAdapter, ControlPanel controlPanel){
		this.dynet = dynet;
		this.appAdapter = appAdapter;
		this.controlPanel = controlPanel;
	}
	
	
	@Override
	public TaskIterator createTaskIterator(View<CyNode> nodeView,
			CyNetworkView networkView) {
		return new TaskIterator(new NodeAnalyzerTask(nodeView, dynet, appAdapter, controlPanel));
	}
	
	@Override
	public boolean isReady(View<CyNode> nodeView, CyNetworkView networkView) {
		return super.isReady(nodeView, networkView) && networkView == dynet.getUnionNetworkView();
	}

}
