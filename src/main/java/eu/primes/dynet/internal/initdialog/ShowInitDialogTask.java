package eu.primes.dynet.internal.initdialog;


import org.cytoscape.app.swing.CySwingAppAdapter;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;

import eu.primes.dynet.internal.DynetActivator;

/**
 * This task will show the initial setup dialog.
 * 
 * @author Ivan Hendy Goenawan
 */

public class ShowInitDialogTask extends AbstractTask {
	private final CySwingAppAdapter appAdapter;
	private final DynetActivator activator;
	
	public ShowInitDialogTask (CySwingAppAdapter appAdapter, DynetActivator activator){
		this.appAdapter = appAdapter;
		this.activator = activator;
	}
	
	public void run(final TaskMonitor taskMonitor) throws Exception{
		taskMonitor.setTitle("Dynet");
		taskMonitor.setStatusMessage("Showing initialisation dialog");
		
		SetupDialog setupDialog = new SetupDialog(appAdapter, activator);
	}
	
}
